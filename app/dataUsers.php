<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class dataUsers extends Model
{
    protected $table = 'data_users';
    protected $filliable = [
    	'idUsuario',
		'cedula',
		'curriculo',
    ];
}
