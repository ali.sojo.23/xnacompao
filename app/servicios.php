<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class servicios extends Model
{
    protected $table = 'servicios';

    protected $fillable = [
    	'servicio',
		'id_pais',
		'costo',
		'costo_profesional'
    ];
    public function pais(){
    	return $this->belongsTo(tasaCambio::class,'id_pais');
    }
}
