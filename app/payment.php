<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
    protected $table ='payments';
    protected $fillable = [
    	'token',
		'payment_method_id',
		'installments',
		'issuer_id',
		'status',
		'mercadopago_id',
		'user_id',
		'payment_type_id'
    ];
}
