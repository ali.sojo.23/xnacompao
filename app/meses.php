<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class meses extends Model
{
    protected $table = 'meses';
    protected $fillable = [
    	'idUsuario',
		'enero',
		'febrero',
		'marzo',
		'abril',
		'mayo',
		'junio',
		'julio',
		'agosto',
		'septiembre',
		'octubre',
		'noviembre',
		'diciembre',
    ];
}
