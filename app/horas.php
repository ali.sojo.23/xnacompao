<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class horas extends Model
{
    protected $table = 'horas';
    protected $fillable = [
    	'idDia',
		'0horas',
		'1horas',
		'2horas',
		'3horas',
		'4horas',
		'5horas',
		'6horas',
		'7horas',
		'8horas',
		'9horas',
		'10horas',
		'11horas',
		'12horas',
		'13horas',
		'14horas',
		'15horas',
		'16horas',
		'17horas',
		'18horas',
		'19horas',
		'20horas',
		'21horas',
		'22horas',
		'23horas',
    ];
}
