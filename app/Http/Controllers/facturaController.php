<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use acompana\User;
use acompana\Pais;
use acompana\Ciudad;
use acompana\services;
use acompana\contratos;
use acompana\factura;
use acompana\costoSanatorio;
use acompana\costoHospital;
use acompana\costoDomicilio;
use acompana\membresiaContrato;

class facturaController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contratos = contratos::all();

    	return view('dashboard.contratos.contratos',compact('contratos'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        $factura = factura::find($id);
        return view('dashboard.factura.membresia',compact('factura'));
/*
    	if($request->tipo == 'membresia'):
            $membresias = membresiaContrato::find($id);

            return view('dashboard.factura.membresia',compact('membresias'));
       /* else :

            return $request;

    	$contrato = contratos::find($id);
    	$user = User::find($ocontrato->user_id);
    	$pais = Pais::where('PaisCodigo',$user->country)->get();
    	$ciudad = Ciudad::where('CiudadID',$user->estado)->get();
    	$factura = factura::where('contrato_id',$contrato->id)->get();
    	$cuidadores = explode("|",$contrato->cuidadores);
    	$usuarios = User::all();
    	// return $contrato;
        return view('dashboard.factura.invoice',compact('contrato','user','pais','ciudad','factura','cuidadores','usuarios'));
        endif;
*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
