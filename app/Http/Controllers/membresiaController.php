<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use acompana\Pais;
use acompana\membresia;
use acompana\membresiaCosto;
use acompana\tasaCambio;
class membresiaController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $membresias = membresia::all();

        return view('dashboard.contabilidad.membresia.index',compact('membresias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pais = tasaCambio::all();
        return view('dashboard.contabilidad.membresia.create',compact('pais'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $membresia = new membresia;
            $membresia->membresia = $request->membresia;
            $membresia->pais_id = $request->idPais;
            if($request->profesional == 1):
                $membresia->profesional = $request->profesional;
            else:
                $membresia->profesional = 0;
            endif;
        $membresia->save();

        return redirect(route('membresia.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $membresia = membresia::find($id);
        return view('dashboard.contabilidad.membresia.edit',compact('membresia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = decrypt($id);
        $membresia = membresia::find($id);
            $membresia->membresia = $request->membresia;
        $membresia->save();
        return redirect(route('membresia.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = decrypt($id);
        $membresia = membresia::destroy($id);
        $costo = membresiaCosto::where('membresias_id',$id)->delete();
        return redirect(route('membresia.index'));
    }
}
