<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use acompana\membresia;
use acompana\Pais;
use acompana\tasaCambio;
use acompana\membresiaCosto;

class membresiaCostoController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costos = membresiaCosto::all();
        return view('dashboard.contabilidad.membresia.costo.index',compact('costos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $id)
    {

        $id = decrypt($id->id);
        $membresia = membresia::find($id);
        $tasas = tasaCambio::where('pais_id',$membresia->pais_id)->get();
        return view('dashboard.contabilidad.membresia.costo.create',compact('membresia','tasas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tasas = $request->tasacambio_id;
        foreach ($tasas as $tasa) {
            $membresia = new membresiaCosto;
                $membresia->membresias_id = $request->membresia;
                $membresia->suscripcion = $request->suscripcion;
                $membresia->mensualidad = $request->mensualidad;
                $membresia->tasaCambios_id = $tasa;
                $membresia->hora = $request->horas;
                $membresia->dias = $request->dias;
            $membresia->save();
        }
        return redirect(route('costoMembresia.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        $costos = membresiaCosto::where('membresias_id',$id)->get();
        return view('dashboard.contabilidad.membresia.costo.index',compact('costos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $costo = membresiaCosto::find($id);
        return view('dashboard.contabilidad.membresia.costo.edit',compact('costo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = decrypt($id);
        $costo = membresiaCosto::find($id);
            $costo->suscripcion = $request->suscripcion;
            $costo->mensualidad = $request->mensualidad;
        $costo->save();
        return redirect(route('costoMembresia.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = decrypt($id);
        $membresia = membresiaCosto::destroy($id);
        return redirect(route('costoMembresia.index'));

    }
}
