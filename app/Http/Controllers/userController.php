<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use acompana\User;
use acompana\Ciudad;
use acompana\Pais;
use acompana\services;
use acompana\workExperiences;

class userController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        //return $users;
        return view('dashboard.users.show',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $paises = Pais::all();
        return view('dashboard.users.create',compact('paises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User($request->all());
        $user->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $id=decrypt($id);
        $user = User::find($id);
        return view('perfilDeUsuario',compact('user'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $users = User::find($id);

            
        if($users->role == 1){
            return view('dashboard.users.acompañante.edit',compact('users'));
        }elseif($users->role == 2){
            return view('dashboard.users.editUser',compact('users'));
        }elseif($users->role == 502||$users->role == 500 || $users->role == 1000){
            return view('dashboard.users.edit',compact('users'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = decrypt($id);
        $user = User::find($id);
        $user->fill($request->except('avatar','id_services'));
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $user->avatar = $name;
            $file->move(public_path().'/users_images/',$name);
        }
        foreach ($request->servicios as $servicios) {
            $servicio = new \acompana\services;
                $servicio->usuarioId = $id;
                $servicio->servicio = $servicios;
            $servicio->save();
        }

        $user->save();

        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = decrypt($id);
        $user = User::find($id)->delete();
        return back();
    }
    
}
