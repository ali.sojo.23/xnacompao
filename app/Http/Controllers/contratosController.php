<?php

namespace acompana\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use acompana\User;
use acompana\Pais;
use acompana\Ciudad;
use acompana\services;
use acompana\contratos;
use acompana\factura;
use acompana\costoSanatorio;
use acompana\costoHospital;
use acompana\costoDomicilio;


class contratosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $ciudades = Ciudad::all();
        return view('dashboard.contratos.booking',compact('ciudades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $fechaInicio = $request->fechaInicioContrato;
        $fechaFin = $request->fechaFinContrato;

        $ciudad = $request->ciudad;
        $servicio = $request->servicio;
        $id = decrypt($id);
        
        $user = user::where([
            ['country',$id],
            ['status','3'],
            ['estado',  $ciudad]
            ])->paginate(10);
        if($servicio == 1){
            $costo = costoDomicilio::where('idPais',$id)->get();
        }elseif($servicio == 2){
            $costo = costoHospital::where('idPais',$id)->get();
        }elseif($servicio == 2){
            $costo = costoSanatorio::where('idPais',$id)->get();
        }
        $ciudades = Ciudad::where('PaisCodigo',$id)->get();
        return view('dashboard.contratos.buscar',compact('user','ciudades', 'ciudad' ,'id','servicio','fechaInicio','fechaFin','costo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $id = decrypt($id);
        
        $descuento = $request->descuento; //--> este descuento debe ser expresado en porcentaje
        $dias = $request->dias *8;
        $costo = $request->costoMonedaLocal;
        $contrato = new contratos;
        $contrato->fill($request->all());
        $contrato->ciudad = $request->estado;
        $contador=0;
        $cuidador;
        if(!empty($request->cuidadores[0])){
        foreach ($request->cuidadores as $cuidadores ) {
            if(empty($cuidador)){
                $cuidador = $cuidadores;
                $contador = $contador+1;
            }else{
                $cuidador = $cuidador . "|" . $cuidadores;
                $contador = $contador+1;
            }
            
        }
        $contrato->user_id = $id;
        $contrato->cuidadores = $cuidador;
        $contrato->save();
        $factura = new factura;
        $factura->contrato_id = $contrato->id;
        $factura->subTotal = $dias * 8 * $costo;
        $factura->tax = $factura->subTotal * 0.16;
        $subTotal = $factura->subTotal + $factura->tax;
        $factura->descuento = $subTotal * $descuento;
        $factura->total = $subTotal - $factura->descuento;
        $factura->estado = 1;
        $factura->monedaLocal = $request->MonedaLocal;
        $factura->save();
        $contrato->factura_id = $factura->id;
        $contrato->save();

        return redirect(route('invoice.show',['invoice'=>encrypt($contrato->id)]));

        }else{
            return back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
