<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->status;

        if($user==1||$user==0){
            

            return redirect(route('showUser',['id'=>encrypt(Auth::user()->id)]));
            //eturn $user;
            //return view('test');
        }elseif ($user==2 || $user == 3) {
            return view('Templates.Dashboard.index');
        }
    }
}
