<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use MercadoPago\SDK;
use MercadoPago\Payment;
use Session;
use acompana\factura;
use acompana\User;
use acompana\payment as pagos;


class payController extends Controller
{
    public function pagosMercadoPago(Request $request){
        // return $request->all();
        $factura = factura::find($request->factura);
    	$amount = $factura->total;
    	$description =  $request->description;
    	$user = User::find($request->user_id);
    	$pais = $request->pais;
    	$token = $request->token;
  		$payment_method_id = $request->payment_method_id;
  		$installments = $request->installments;
  		$issuer_id = $request->issuer_id;
  		$email = $user->email;

        if($pais == 'ARG') : 
     	  $mp = SDK::setAccessToken(env('MERCADOPAGO_CLIENT_SECRET_AR'));
        elseif($pais == 'URY') :
            $mp = SDK::setAccessToken(env('MERCADOPAGO_CLIENT_SECRET_UY'));
        elseif($pais == 'VEN') :
            $mp = SDK::setAccessToken(env('MERCADOPAGO_CLIENT_SECRET_VE'));
        elseif($pais == 'BRA') :
            $mp = SDK::setAccessToken(env('MERCADOPAGO_CLIENT_SECRET_BR'));
        elseif($pais == 'COL') :
            $mp = SDK::setAccessToken(env('MERCADOPAGO_CLIENT_SECRET_CO'));
        elseif($pais == 'MXN') :
            $mp = SDK::setAccessToken(env('MERCADOPAGO_CLIENT_SECRET_MX'));
        endif;
    	//...
    	$payment = new Payment();
    	   $payment->transaction_amount = $amount;
    	   $payment->token = $token;
    	   $payment->installments = $installments;
    	   $payment->payment_method_id = $payment_method_id;
    	   $payment->issuer_id = $issuer_id;
           $payment->payer = array(
                "email" => $email
            );
    	// Guarda y postea el pago
    	$payment->save();
    	if($payment->status == 'approved'){
    		$pago = new pagos;
    		$pago->token = $token;
			$pago->payment_method_id = $payment->payment_method_id;
			$pago->installments = $installments;
			$pago->issuer_id = $issuer_id;
			$pago->mercadopago_id = $payment->id;
			$pago->user_id = $user->id;
			$pago->payment_type_id = $payment->payment_type_id;
			$pago->save();

    		$factura = factura::find($request->factura);
    		$factura->estado = 2;
    		$factura->payments_id = $pago->id;
    		$factura->save();
    		// return back();
    	}else return back();
   		//...
    	// Imprime el estado del pago
    	// echo $payment->status;
    	


    }
}
