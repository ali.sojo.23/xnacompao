<?php

namespace acompana\Http\Controllers\app;

use Illuminate\Http\Request;
use acompana\Http\Controllers\Controller;
use acompana\tasaCambio;
use acompana\membresiaCosto;
use acompana\membresiaContrato;
use acompana\factura;
use acompana\Pais;
use acompana\User;

class atencionCliente extends Controller
{
	// `Funcion para administrar los inicios de sesion
	public function __construct()
    {
        $this->middleware('auth');

    }
    // `Funicion para iniciar el proceso de ventas (usuario registrado o no)
     public function membresiaVentasIndex(){
        return view('dashboard.atencion_al_cliente.membresia.contrato.index');
    }
    // Funcion para selecionar la membresia profesional o no y usuario registrado
    public function membresiaVentasPais(Request $request){

            $membresias = membresiaCosto::where('tasaCambios_id',$request->pais)->with(['membresia' => function ($query) {
            					global $request;
    							$query->where('profesional', $request->membresia);
									}])->get();
            $usuario = $request->usuario;
            return view('dashboard.atencion_al_cliente.membresia.index',compact('membresias','usuario'));

    }
    //Muestras las mebresias disponibles y la opcion de contratar
    public function membresiaVentasShow(){
    	$usuarios = User::all();
    	$pais = tasaCambio::all();
    	return view('dashboard.atencion_al_cliente.membresia.booking',compact('usuarios','pais'));
    }
    public function resumenVentas($id){

        $ventas = membresiaContrato::where('vendedor_id',$id)->get();
        foreach ($ventas as $venta ) {
            $facturas[] = factura::where('membresia_contrato_id',$venta->id)->get();
        }
        // return $facturas;
        // foreach ($facturas as $factura) {
        //     echo $factura->id . " <br>";
        // }
        return view('dashboard.atencion_al_cliente.membresia.ingresos.index',compact('facturas'));
    }
}
