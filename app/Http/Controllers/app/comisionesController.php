<?php

namespace acompana\Http\Controllers\app;

use Illuminate\Http\Request;
use acompana\Http\Controllers\Controller;
use acompana\membresiaCosto;
use acompana\tasaCambio;

class comisionesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function mostrarComisiones(){
    	$comisiones = membresiaCosto::All();
    	$comisiones = $comisiones->groupBy('tasaCambios_id');

    	$paises = array_keys($comisiones->toArray());

    	$paises = tasaCambio::whereIn('id',$paises)->get();

    	// return $comisiones;
    	return view('dashboard.comisiones.individual.index',compact('paises','comisiones'));
    }
    public function mostrarComisionesCallCenter(){
        $comisiones = membresiaCosto::All();
        $comisiones = $comisiones->groupBy('tasaCambios_id');

        $paises = array_keys($comisiones->toArray());

        $paises = tasaCambio::whereIn('id',$paises)->get();

        // return $comisiones;
        return view('dashboard.comisiones.call_center.index',compact('paises','comisiones'));
    }
    public function mostrarComisionesReferidos(){
        $comisiones = membresiaCosto::All();
        $comisiones = $comisiones->groupBy('tasaCambios_id');

        $paises = array_keys($comisiones->toArray());

        $paises = tasaCambio::whereIn('id',$paises)->get();

        // return $comisiones;
        return view('dashboard.comisiones.referidos.index',compact('paises','comisiones'));
    }

}
