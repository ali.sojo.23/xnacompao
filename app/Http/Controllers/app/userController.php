<?php

namespace acompana\Http\Controllers\app;

use Illuminate\Http\Request;
use acompana\Http\Controllers\Controller;
use acompana\User;
use acompana\dataUsers;
use acompana\workExperiences;
use acompana\Pais;
use acompana\horas;
use acompana\dias;
use acompana\meses;
use acompana\services;
use acompana\sanatorios;
use acompana\hospitales;
use acompana\domicilios;
use acompana\descripcion;
use acompana\tasaCambio;
use acompana\membresiaCosto;
use Input;
use Validator;

class userController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function moderation(){
        $users = User::all();
        #UsuariosPendientesPorModeracion
        return view('dashboard.users.showModerate',compact('users'));
    }
    public function register(){
        $users = User::all();
        #UsuariosPendientesPorCompletarLosRegistros
        return view('dashboard.users.showVerify',compact('users'));
    }
    public function verification(){
        $users = User::all();
        #UsuariosPendientesPorVerificarCorreo
        return view('dashboard.users.showEmailEmpty',compact('users'));
    }
    public function endModerate(Request $request, $id){
        $user = User::find($id);
        $user->status = $request->status;
        $user->save();

        return redirect(route('moderations'));
    }
    public function docsUpdate($id){
        $user = User::find($id);

        if(!empty($user->id_docs)) :
            return back();
        else :
            return view('dashboard.users.uploadFile');
        endif;

    }
    public function docsStore(Request $request, $id){
        
        $id = decrypt($id);
        $user = User::find($id);
        $dataUser = dataUsers::where('idUsuario',$user->id)->delete();
        $validator = Validator::make($request->all(), [
            'cedula' => 'required|max:4000|mimes:jpeg',
            'curriculo' => 'required|max:4000|mimes:pdf',
        ]);
       

        if($validator->fails()){
            return back()
                ->withErrors( $validator );
        }else{
           $dataUser = new dataUsers;

           $dataUser->idUsuario = $user->id;
           if($request->hasFile('cedula')){
                $file = $request->file('cedula');
                $name = time().$file->getClientOriginalName();
                $dataUser->cedula = $name;
                $file->move(public_path().'/data/users/cedula/',$name);
           }
           if($request->hasFile('curriculo')){
                $file = $request->file('curriculo');
                $name = time().$file->getClientOriginalName();
                $dataUser->curriculo = $name;
                $file->move(public_path().'/data/users/curriculo/',$name);
           }
           
           $dataUser->save();
           $user->id_docs = $dataUser->id;
           $user->save();
           return redirect(route('home'));
        }

    }
    public function workExperiencesEdit($id){
        $pais = Pais::all();
        return view('dashboard.users.workExperience',compact('pais'));
    
    }
    public function workExperiencesPublic(Request $request, $id){
        $id = decrypt($id);
        $user = User::find($id);
        $work = workExperiences::all();
        foreach ($work as $worked) {
            if($worked->idUser == $user->id){
                $worked->delete();
            }
        }

        $work = new workExperiences;
        $work->fill($request->all());
        $work->idUser = $user->id;
        $work->save();
        $user->id_workExperience = $work->id;
        $user->save();


        return redirect(route('home'));

        

    }
    public function horariosSanatorios($id){

        return view('dashboard.users.disponibilidadSanatorio');

    }
    public function horariosSanatoriosUpdate(Request $request, $id){

        
        $id = decrypt($id);
        $user = User::find($id);
        $servicios = services::all();
        $sanatorio = sanatorios::all();
        foreach ($servicios as $servicios) {
            if($servicios->usuarioId == $user->id){
                foreach ($sanatorio as $sanatorio) {
                    if($sanatorio->id == $servicios->sanatorios){
                        $sanatorio->delete();
                    }
                }
            }else{
                $servicios = new services;
            }
        }
        
        $sanatorio =  new sanatorios;
            $meses = new meses;
            $meses->fill($request->all());
            $meses->idUsuario = $user->id;
            $meses->save();
        $sanatorio->mes = $meses->id;
            $dias = new dias;
            $dias->fill($request->all());
            $dias->idUsuario = $user->id;
            $dias->save();
        $sanatorio->dia = $dias->id;
            $hora = new horas;
            $hora->fill($request->all());
            $hora->idDia = $user->id;
            $hora->save();
        $sanatorio->hora = $hora->id;
        $sanatorio->save();
        $servicios->sanatorios = $sanatorio->id;
        $servicios->save();
        $user->id_services = $servicios->id;
        $user->save();
        return redirect(route('home'));
    }
    public function horariosHospitales($id){

        return view('dashboard.users.disponibilidadHospital');

    }
    public function horariosHospitalesUpdate(Request $request, $id){

        
        $id = decrypt($id);
        $user = User::find($id);
        $servicios = services::all();
        $hospital = hospitales::all();
        foreach ($servicios as $servicios) {
            if($servicios->usuarioId == $user->id){
                foreach ($hospital as $hospital) {
                    if($hospital->id == $servicios->hospitales){
                        $hospital->delete();
                    }
                }
            }else{
                $servicios = new services;
            }
        }
        
        $hospital =  new hospitales;
            $meses = new meses;
            $meses->fill($request->all());
            $meses->idUsuario = $user->id;
            $meses->save();
        $hospital->mes = $meses->id;
            $dias = new dias;
            $dias->fill($request->all());
            $dias->idUsuario = $user->id;
            $dias->save();
        $hospital->dia = $dias->id;
            $hora = new horas;
            $hora->fill($request->all());
            $hora->idDia = $user->id;
            $hora->save();
        $hospital->hora = $hora->id;
        $hospital->save();
        $servicios->hospitales = $hospital->id;
        $servicios->save();
        $user->id_services = $servicios->id;
        $user->save();
        return redirect(route('home'));
    }
    public function horariosDomicilios($id){

        return view('dashboard.users.disponibilidadDomicilio');

    }
    public function horariosDomiciliosUpdate(Request $request, $id){

        
        $id = decrypt($id);
        $user = User::find($id);
        $servicios = services::all();
        $domicilio = domicilios::all();
        foreach ($servicios as $servicios) {
            if($servicios->usuarioId == $user->id){
                foreach ($domicilio as $domicilio) {
                    if($domicilio->id == $servicios->domicilios){
                        $domicilio->delete();
                    }
                }
            }else{
                $servicios = new services;
            }
        }
        
        $domicilio =  new domicilios;
            $meses = new meses;
            $meses->fill($request->all());
            $meses->idUsuario = $user->id;
            $meses->save();
        $domicilio->mes = $meses->id;
            $dias = new dias;
            $dias->fill($request->all());
            $dias->idUsuario = $user->id;
            $dias->save();
        $domicilio->dia = $dias->id;
            $hora = new horas;
            $hora->fill($request->all());
            $hora->idDia = $user->id;
            $hora->save();
        $domicilio->hora = $hora->id;
        $domicilio->save();
        $servicios->domicilios = $domicilio->id;
        $servicios->save();
        $user->id_services = $servicios->id;
        $user->save();
        return redirect(route('home'));
    }
    public function descripcion(Request $request, $id){

        $id = decrypt($id);
        $descripcion = descripcion::where('users_id',$id)->delete();
        
        $descripcion = new descripcion($request->all());
        return back();
    }

}
