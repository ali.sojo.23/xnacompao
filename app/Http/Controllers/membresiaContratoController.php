<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use acompana\membresiaCosto;
use acompana\membresiaContrato;
use acompana\factura;

class membresiaContratoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $membresia = membresiaCosto::all();
        return view('dashboard.membresias.index',compact('membresia'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $membresia = new membresiaContrato;
            $membresia->vendedor_id = $request->ATC;
            $membresia->membresia_costo_id = $request->membresiaCosto;
            $membresia->user_id = $request->usuario;
            $membresia->dias_disponible = $request->dias_disponible;
        $membresia->save();
        
        $factura = new factura;
            $factura->membresia_contrato_id = $membresia->id;
            $factura->subTotal = $membresia->costo->suscripcion*$membresia->costo->cambio->tasaCambios;
            $factura->tax = $membresia->costo->cambio->tax*$membresia->costo->suscripcion*$membresia->costo->cambio->tasaCambios;
            $factura->descuento = 0;
            $factura->total = $factura->tax + $factura->subTotal;
            $factura->estado = 1;
            if($request->ATC != 0){  $factura->destino = 1;  }
            $factura->monedaLocal = $membresia->costo->cambio->monedaLocal;
        $factura->save();



        return redirect(route('invoice.show',['invoice'=>encrypt($factura->id)]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        $membresias = membresiaContrato::where('user_id',$id)->get();
        return view('dashboard.membresias.show',compact('membresias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
