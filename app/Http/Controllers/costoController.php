<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use acompana\User;
use acompana\Pais;
use acompana\costoDomicilio;
use acompana\costoHospital;
use acompana\costoSanatorio;

class costoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $pais = pais::all();
        return view('dashboard.contabilidad.crearCostos',compact('pais'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->servicio == 1){
            $costoSanatorio = costoSanatorio::all();
            foreach ($costoSanatorio as $costoSanatorio) {
                if($request->idPais == $costoSanatorio->idPais){
                    $costoSanatorio->delete();
                }
            }
            $costoSanatorio = new costoSanatorio;
            $costoSanatorio->fill($request->all())->save();
            return redirect(route('costo.show',['costo'=>encrypt(1)]));
        }elseif($request->servicio == 2){
            $costoHospital = costoHospital::all();
            foreach ($costoHospital as $costoHospital) {
                if($request->idPais == $costoHospital->idPais){
                    $costoHospital->delete();
                }
            }
            $costoHospital = new costoHospital;
            $costoHospital->fill($request->all())->save();
            return redirect(route('costo.show',['costo'=>encrypt(2)]));
        }elseif($request->servicio == 3){
            $costoDomicilio = costoDomicilio::all();
            foreach ($costoDomicilio as $costoDomicilio) {
                if($request->idPais == $costoDomicilio->idPais){
                    $costoDomicilio->delete();
                }
            }
            $costoDomicilio = new costoDomicilio;
            $costoDomicilio->fill($request->all())->save();
            return redirect(route('costo.show',['costo'=>encrypt(3)]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        #sanatorios
        if($id == 1){
            $costo = costoSanatorio::all();
            return view('dashboard.contabilidad.costoSanatorio',compact('costo'));
        }
        #hospitales
        elseif($id == 2){
            $costo = costoHospital::all();
            return view('dashboard.contabilidad.costoHospital',compact('costo'));
        }
        #domicilios
        elseif($id == 3){
            $costo = costoDomicilio::all();
            return view('dashboard.contabilidad.costoDomicilio',compact('costo'));

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
