<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use acompana\tasaCambio;
use acompana\servicios;

class serviciosController extends Controller
{
         public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = servicios::all();
        return view('dashboard.contabilidad.servicios.index',compact('servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises = tasaCambio::all();
        return view('dashboard.contabilidad.servicios.create',compact('paises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $servicios = new servicios($request->all());
        $servicios->save();
        return redirect(route('servicios.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $servicio = servicios::find($id);
        return view('dashboard.contabilidad.servicios.edit',compact('servicio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = decrypt($id);
        $servicio = servicios::find($id);
            $servicio->servicio = $request->servicio;
            $servicio->costo = $request->costo;
            $servicio->costo_profesional = $request->costo_profesional;
        $servicio->save();
        return redirect(route('servicios.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        servicios::destroy(decrypt($id));
        return back();
    }
}
