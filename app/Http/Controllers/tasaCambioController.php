<?php

namespace acompana\Http\Controllers;

use Illuminate\Http\Request;
use acompana\Pais;
use acompana\tasaCambio;

class tasaCambioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasas = tasaCambio::all();
        return view('dashboard.contabilidad.tasaCambio.index',compact('tasas'));
    }

    /*
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pais = Pais::all();

        return view('dashboard.contabilidad.tasaCambio.create',compact('pais'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tasa = new tasaCambio();
            $tasa->pais_id = $request->idPais;
            $tasa->tasaCambios = $request->tasaCambio;
            $tasa->monedaLocal = $request->monedaLocal;
            $tasa->tax = $request->tax;
        $tasa->save();

        return redirect(route('tasadecambio.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $tasa = tasaCambio::find($id);
        return view('dashboard.contabilidad.tasaCambio.edit',compact('tasa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = decrypt($id);
        $tasa = tasaCambio::find($id);
            $tasa->tasaCambios = $request->tasaCambio;
            $tasa->monedaLocal = $request->monedaLocal;
            $tasa->tax = $request->tax/100;
        $tasa->save();

        return redirect(route('tasadecambio.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = decrypt($id);
        $tasa = tasaCambio::destroy($id);
        return redirect(route('tasadecambio.index'));
    }
}
