<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class costoHospital extends Model
{
    protected $table = 'costoHospital';
    protected $fillable = [
    	'idPais',
		'costoDolar',
		'tasaCambio',
		'costoLocal',
		'monedaLocal'
    ];

}
