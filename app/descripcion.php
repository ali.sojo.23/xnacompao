<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class descripcion extends Model
{
    protected $table = 'descripcions';
    protected $fillable = [
    	'users_id',
    	'descripcion'
    ]
}
