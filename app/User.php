<?php

namespace acompana;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lastName',
        'email',
        'password',
        'phone',
        'birthDay',
        'company',
        'country',
        'position' ,
        'verifyToken',
        'terms',
        'status',
        'tokenSocialite',
        'avatar',
        'role',
        'id_services',
        'idWorkExperience',
        'sexo',
        'cedula',
        'coberturaMedica',
        'emergenciaMovil',
        'estado',
        'id_docs',
        'idMeses',
        'idDias',
        'idHoras',


    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function setPasswordAttribute($valor){
        if(!empty($valor)){
            $this->attributes['password'] = \Hash::make($valor);
        }
    }
    
    public function servicio(){
        return $this->belongsTo(services::class,'id_services');
    }
    public function estados(){
        return $this->belongsTo(Ciudad::class,'estado','CiudadID');
    }
    public function pais(){
        return $this->belongsTo(Pais::class,'country','PaisCodigo');
    }
    public function descripcion(){
        return $this->belongsTo(descripcion::class,'users_id');
    }

    public function scopeName($query,$ciudad){
        $query->where('estado',$ciudad);
    }


}
