<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class contratos extends Model
{
    protected $table = 'contratos';
    protected $fillable = [
		'fechaInicio',
		'fechaFin',
		'horario_id',
		'ciudad',
		'cuidadores',
		'servicio',
		'factura_id',
		'user_id',
    ];

    public function factura(){
        return $this->belongsTo(factura::class,'factura_id');
    }
     public function usuario(){
        return $this->belongsTo(User::class,'user_id');
    }
}
