<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudad';
    protected $fillable = [
    	'CiudadID',
    	'CiudadNombre',
    	'PaisCodigo',
    ];
}
