<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class workExperiences extends Model
{
	protected $table = 'work_experiences';
    protected $fillable = [
    	'idUser',
		'work1',
		'countryWork1',
		'inicioWork1',
		'finWork1',
		'cargoWork1',
		'work2',
		'countryWork2',
		'inicioWork2',
		'finWork2',
		'cargoWork2',
		'work3',
		'countryWork3',
		'inicioWork3',
		'finWork3',
		'cargoWork3',
    ];
}
