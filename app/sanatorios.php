<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class sanatorios extends Model
{
    protected $table ='sanatorios';
    protected $fillable =[
    	'dia',
		'mes',
		'hora'
    ];
    public function diasLaborales(){
    	return $this->belongsTo(dias::class,'dia');
    }
    public function mesesLaborales(){
    	return $this->belongsTo(meses::class,'mes');
    }
    public function horasLaborales(){
    	return $this->belongsTo(horas::class,'hora');
    }
}

