<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class hospitales extends Model
{
    protected $table ='hospitales';
    protected $fillable =[
    	'dia',
		'mes',
		'hora'
    ];
    public function diasLaborales(){
    	return $this->belongsTo(dias::class,'dia');
    }
    public function mesesLaborales(){
    	return $this->belongsTo(meses::class,'mes');
    }
    public function horasLaborales(){
    	return $this->belongsTo(horas::class,'hora');
    }
}
