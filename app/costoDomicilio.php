<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class costoDomicilio extends Model
{
    protected $table = 'costoDomicilio';
    protected $fillable = [
    	'idPais',
		'costoDolar',
		'tasaCambio',
		'costoLocal',
		'monedaLocal'
    ];
}
