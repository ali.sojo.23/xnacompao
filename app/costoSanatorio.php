<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class costoSanatorio extends Model
{
    protected $table = 'costoSanatorio';
    protected $fillable = [
    	'idPais',
		'costoDolar',
		'tasaCambio',
		'costoLocal',
		'monedaLocal'
    ];

}
