<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
	protected $table = 'pais';
     protected $fillable = [
       'PaisCodigo',
       'PaisNombre',
       'PaisContinente',
       'PaisRegion',
    ];

   public function ciudades(){
        return $this->hasMany(Ciudad::class,'PaisCodigo','PaisCodigo');
    }
   public function tasaCambio(){
        return $this->belongsTo(tasaCambio::class,'PaisCodigo','pais_id');
    }
}
