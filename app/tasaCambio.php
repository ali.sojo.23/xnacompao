<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class tasaCambio extends Model
{
    protected $table = 'tasa_cambios';
	protected $fillable = [
		'pais_id',
		'tasaCambios',
		'monedaLocal'
	];
	public function Pais(){
    	return $this->belongsTo(Pais::class,'pais_id','PaisCodigo');
    }
    public function servicios(){
        return $this->hasMany(servicios::class,'id_pais');
    }
}
