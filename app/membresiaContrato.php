<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class membresiaContrato extends Model
{
    protected $table = 'membresia_contratos';
    protected $fillable = [
		'membresia_costo_id',	
		'user_id',
        'dias_disponible',
        'vendedor_id'
    ];
    public function costo(){
        return $this->belongsTo(membresiaCosto::class,'membresia_costo_id');
    }
    public function usuario(){
    	return $this->belongsTo(User::class,'user_id');
    }
}
