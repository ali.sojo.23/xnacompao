<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class membresiaCosto extends Model
{
    protected $table = 'membresia_costos';
	
	protected $fillable = [
		'membresias_id',
		'hora',
		'dias',
		'suscripcion',
		'mensualidad',
		'tasaCambios_id'
	];

	public function cambio(){
    	return $this->belongsTo(tasaCambio::class,'tasaCambios_id');
    }
    public function membresia(){
    	return $this->belongsTo(membresia::class,'membresias_id');
    }
}
