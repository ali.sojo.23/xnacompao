<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class services extends Model
{
    protected $table = 'services';

    protected $fillable = [
    	'usuarioId',
    	'sanatorios',
		'hospitales',
		'domicilios',
    ];

    public function servicioSanatorio(){
    	return $this->belongsTo(sanatorios::class,'sanatorios');
    }
    public function servicioHospital(){
    	return $this->belongsTo(hospitales::class,'hospitales');
    }
    public function servicioDomicilio(){
    	return $this->belongsTo(domicilios::class,'domicilios');
    }
}
