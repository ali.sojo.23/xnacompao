<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class dias extends Model
{
    protected $table = 'dias';
    protected $fillable = [
    	'idUsuario',
		'lunes',
		'martes',
		'miercoles',
		'jueves',
		'viernes',
		'sabado',
		'domingo'
    ];
}
