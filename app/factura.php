<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class factura extends Model
{
    protected $table = 'facturas';
    protected $fillable = [
    	'membresia_contrato_id',
		'servicio_contrato_id',
		'total',
		'subTotal',
		'tax',
		'descuento',
		'estado',
		'monedaLocal',
		'destino'
    ];
    public function membresia(){
    	return $this->belongsTo(membresiaContrato::class,'membresia_contrato_id');
    }
    
}
