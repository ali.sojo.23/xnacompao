<?php

namespace acompana;

use Illuminate\Database\Eloquent\Model;

class membresia extends Model
{
    protected $table = 'membresias';
	protected $fillable = [
		'membresia',
		'pais_id',
        'profesional'
	];
	public function Pais(){
    	return $this->belongsTo(Pais::class,'pais_id','PaisCodigo');
    }
    public function costo(){
    	return $this->hasMany(membresiaCosto::class);
    }
}
