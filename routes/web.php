<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','frontController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('verifyEmailFirst','Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');

Route::get('verify/{email}/{verifyToken}','Auth\RegisterController@sendEmailDone')->name('sendEmailDone');
Route::get('home/users/{id}/edit','userController@edit')->name('showUser');
Route::resource('home/users','userController');
Route::get('home/users/profile/{id}','userController@show')->name('showProfile');
Route::get('home/moderations/users','app\userController@moderation')->name('moderations');
Route::get('home/register/users','app\userController@register')->name('create');
Route::get('home/verifications/users','app\userController@verification')->name('verifications');
Route::PUT('home/moderation/{id}','app\userController@endModerate')->name('endModerate');
Route::get('home/moderation/uploadfile/{id}','app\userController@docsUpdate')->name('docsUpdate');
Route::PUT('home/moderation/uploadfile/{id}','app\userController@docsStore')->name('docsStore');

Route::get('home/users/profile/workexperience/{id}','app\userController@workExperiencesEdit')->name('workExperienceEdit');
Route::PUT('home/users/profile/workexperience/{id}','app\userController@workExperiencesPublic')->name('workExperiencePublic');
//Horarios de trabajo Sanatorio
Route::get('home/users/profile/horario/sanatorio/{id}','app\userController@horariosSanatorios')->name('horariosSanatorios');
Route::PUT('home/users/profile/horario/sanatorio/{id}','app\userController@horariosSanatoriosUpdate')->name('horariosSanatoriosUpdate');
//Horaio de trabajo Hospital
Route::get('home/users/profile/horario/hospital/{id}','app\userController@horariosHospitales')->name('horariosHospital');
Route::PUT('home/users/profile/horario/hospital/{id}','app\userController@horariosHospitalesUpdate')->name('horariosHospitalUpdate');
#Horario de trabajo en Domicilio
Route::get('home/users/profile/horario/domicilio/{id}','app\userController@horariosDomicilios')->name('horariosDomicilio');
Route::PUT('home/users/profile/horario/domicilio/{id}','app\userController@horariosDomiciliosUpdate')->name('horariosDomicilioUpdate');
// Redirect the user to the provider authentication page
Route::get('auth/facebook', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('auth/facebook/callback', 'Auth\SocialAuthController@handleProviderCallback');
#
#Routas para el costo de los servicios
Route::resource('/home/contabilidad/costo','costoController'); 
#
#Routes para Manejo de las tasas de cambios
Route::resource('/home/contabilidad/tasadecambio','tasaCambioController');
#Route para el manejo de las membresias
Route::resource('/home/productos/membresia','membresiaController');
#Ruta para manejo de Servicios
Route::resource('/home/productos/servicios','serviciosController');
#Route para manejo de los costos de las membresias
Route::resource('/home/contabilidad/costoMembresia','membresiaCostoController');
#Routes para las contrataciones de servicios
Route::resource('/home/contrato','contratosController');
#
#Contrat de Membresias
Route::resource('/home/productos/contrato/membresias','membresiaContratoController');
#
Route::resource('/home/invoice','facturaController');
#
#Routes para los pagos MercadoPago y PayPal
Route::post('/home/invoice/payout','payController@pagosMercadoPago')->name('recibirPagos');




#seecion de desarrollo para atencion al cliente
Route::get('/home/atencion_al_cliente/membresias','app\atencionCliente@membresiaVentasIndex')->name('ventaMembresia');
Route::get('/home/atencion_al_cliente/ventas/membresias','app\atencionCliente@membresiaVentasShow')->name('ventaMembresiaUsuario');
Route::GET('/home/atencion_al_cliente/ventas/membresias/usuario_registrado/','app\atencionCliente@membresiaVentasPais')->name('ventaMembresiaPais');

// Seccion de contabilidad para atencion al cliente
Route::get('/home/contabilidad/atencion_al_cliente/membresias/{id}/vendedor','app\atencionCliente@resumenVentas')->name('resumenVentas');

//SEccion de Comisiones
Route::get('/home/comisiones','app\comisionesController@mostrarComisiones')->name('comisiones.show');
Route::get('/home/comisiones/call_center','app\comisionesController@mostrarComisionesCallCenter')->name('comisionesCall.show');
Route::get('/home/comisiones/referidos','app\comisionesController@mostrarComisionesReferidos')->name('comisionesReferidos.show');
