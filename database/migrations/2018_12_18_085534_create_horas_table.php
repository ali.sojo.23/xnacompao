<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idDia');
            $table->boolean('0horas')->nullable();
            $table->boolean('1horas')->nullable();
            $table->boolean('2horas')->nullable();
            $table->boolean('3horas')->nullable();
            $table->boolean('4horas')->nullable();
            $table->boolean('5horas')->nullable();
            $table->boolean('6horas')->nullable();
            $table->boolean('7horas')->nullable();
            $table->boolean('8horas')->nullable();
            $table->boolean('9horas')->nullable();
            $table->boolean('10horas')->nullable();
            $table->boolean('11horas')->nullable();
            $table->boolean('12horas')->nullable();
            $table->boolean('13horas')->nullable();
            $table->boolean('14horas')->nullable();
            $table->boolean('15horas')->nullable();
            $table->boolean('16horas')->nullable();
            $table->boolean('17horas')->nullable();
            $table->boolean('18horas')->nullable();
            $table->boolean('19horas')->nullable();
            $table->boolean('20horas')->nullable();
            $table->boolean('21horas')->nullable();
            $table->boolean('22horas')->nullable();
            $table->boolean('23horas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horas');
    }
}
