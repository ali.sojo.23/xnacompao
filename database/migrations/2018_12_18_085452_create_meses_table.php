<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idUsuario');
            $table->boolean('enero')->nullable;
            $table->boolean('febrero')->nullable;
            $table->boolean('marzo')->nullable;
            $table->boolean('abril')->nullable;
            $table->boolean('mayo')->nullable;
            $table->boolean('junio')->nullable;
            $table->boolean('julio')->nullable;
            $table->boolean('agosto')->nullable;
            $table->boolean('septiembre')->nullable;
            $table->boolean('octubre')->nullable;
            $table->boolean('noviembre')->nullable;
            $table->boolean('diciembre')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meses');
    }
}
