<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostoHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costoHospital', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idPais');
            $table->decimal('costoDolar');
            $table->decimal('tasaCambio');
            $table->decimal('costoLocal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costoHospital');
    }
}
