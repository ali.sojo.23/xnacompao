<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUser')->nullable();
            #work1
            $table->string('work1')->nullable();
            $table->string('countryWork1')->nullable();
            $table->datetime('inicioWork1')->nullable();
            $table->datetime('finWork1')->nullable();
            $table->string('cargoWork1')->nullable();
            #work2
            $table->string('work2')->nullable();
            $table->string('countryWork2')->nullable();
            $table->datetime('inicioWork2')->nullable();
            $table->datetime('finWork2')->nullable();
            $table->string('cargoWork2')->nullable();
            #work3
            $table->string('work3')->nullable();
            $table->string('countryWork3')->nullable();
            $table->datetime('inicioWork3')->nullable();
            $table->datetime('finWork3')->nullable();
            $table->string('cargoWork3')->nullable();
            #endWork
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_experiences');
    }
}
