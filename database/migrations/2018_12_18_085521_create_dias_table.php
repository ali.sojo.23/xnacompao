<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idUsuario');
            $table->boolean('lunes')->nullable;
            $table->boolean('martes')->nullable;
            $table->boolean('miercoles')->nullable;
            $table->boolean('jueves')->nullable;
            $table->boolean('viernes')->nullable;
            $table->boolean('sabado')->nullable;
            $table->boolean('domingo')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dias');
    }
}
