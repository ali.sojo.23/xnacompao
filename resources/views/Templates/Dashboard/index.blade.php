<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<title>Bienvenido {{Auth::user()->name}} {{Auth::user()->lastName}} a acompaño.com | Dashboard </title>


<!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<meta name="author" content="codedthemes" />

<link rel="icon" href="{{asset('files/assets/images/favicon.png')}}" type="image/png">

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/bootstrap/css/bootstrap.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/themify-icons/themify-icons.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/icofont/css/icofont.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/font-awesome/css/font-awesome.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/prism/prism.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('files/assets/css/jquery.mCustomScrollbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/switchery/css/switchery.min.css')}}">

</head>

<body>

<div class="theme-loader">
<div class="loader-track">
<div class="loader-bar"></div>
</div>
</div>

<div id="pcoded" class="pcoded">
<div class="pcoded-overlay-box"></div>
<div class="pcoded-container navbar-wrapper">
<!-- header -->
@include('Templates.Dashboard.assets.header')
<!-- sidebarchat -->
@include('Templates.Dashboard.assets.chat.sidebarChat')
<!-- Show Chat-->
@include('Templates.Dashboard.assets.chat.showChat')
<div class="pcoded-main-container">
<div class="pcoded-wrapper">
<!-- NavBAr -->
@include('Templates.Dashboard.assets.sidebar')
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<!-- Header Dashboard
@include('Templates.Dashboard.assets.body.header')
-->
<!-- Body Dashboard -->
@include('Templates.Dashboard.assets.body.body')
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>








<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="files/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="files/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="files/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="files/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="files/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->


<script src="{{asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
<script src="{{asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
<script src="{{asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

<script src="{{asset('files/bower_components/modernizr/js/modernizr.js')}}"></script>
<script src="{{asset('files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>

<script src="{{asset('files/assets/pages/prism/custom-prism.js')}}"></script>

<script src="{{asset('files/assets/js/pcoded.min.js')}}"></script>
<script src="{{asset('files/assets/js/vertical/menu/menu-sidebar-fixed.js')}}"></script>
<script src="{{asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{asset('files/assets/js/script.js')}}"></script>
<script src="{{asset('files/bower_components/switchery/js/switchery.min.js')}}"></script>
<script src="{{asset('files/assets/pages/advance-elements/swithces.js')}}"></script>

@include('Templates.Dashboard.assets.footer')

</body>
</html>
