@if(Auth::user()->role == 1000)
<div class="pcoded-navigation-label">Administración de usuarios</div>
<ul class="pcoded-item pcoded-left-item">
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-user"></i><b>U</b></span>
<span class="pcoded-mtext">Usuarios</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('users.create')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Crear nuevo usuario</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('users.index')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Mostrar Usuarios</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<!-- new
<li class="">
<a href="">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Analytics</span>
<span class="pcoded-badge label label-info ">NEW</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
-->
</ul>
</li>
</ul>
@endif