@if(Auth::user()->role == 1000 || Auth::user()->role == 2)
<div class="pcoded-navigation-label">Perfil Cliente</div>
<ul class="pcoded-item pcoded-left-item">
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-user"></i><b>U</b></span>
<span class="pcoded-mtext">Mi Perfil</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<?php 
if(empty(Auth::user()->id_docs)){
 ?>
<li class="">
<a href="{{route('docsUpdate',['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Documentos de Verificación</span>
<span class="pcoded-badge label label-info ">*</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<?php } ?>
<li class="">
<a href="{{route('membresias.show',['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Mis Membresias</span>
<span class="pcoded-badge label label-info ">New</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>
</li>
</ul>
@endif