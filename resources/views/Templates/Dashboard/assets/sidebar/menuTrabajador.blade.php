@if(Auth::user()->role == 1000 || Auth::user()->role == 1)
<div class="pcoded-navigation-label">Perfil de Cuidador</div>
<ul class="pcoded-item pcoded-left-item">
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-user"></i><b>U</b></span>
<span class="pcoded-mtext">Mi Perfil</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="pcoded-hasmenu">
<a href=javascript:void(0)"">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Disponibilidad de Horario</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('horariosSanatorios',['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Sanatorios</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('horariosHospital',['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Hospitales</span>
<span class="pcoded-mcaret"></span>
</a>
</li>

<li class="">
<a href="{{route('horariosDomicilio',['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Domicilios</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>
</li>
<li class="">
<a href="{{route('workExperienceEdit', ['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Experiencia laboral</span>
<span class="pcoded-badge label label-info ">NEW</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<?php 
if(empty(Auth::user()->id_docs)){
 ?>
<li class="">
<a href="{{route('docsUpdate',['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Documentos de Verificación</span>
<span class="pcoded-badge label label-info ">*</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<?php } ?>
</ul>
</li>
</ul>
@endif