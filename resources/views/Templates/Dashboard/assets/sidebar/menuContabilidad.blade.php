@if(Auth::user()->role == 1000)
<div class="pcoded-navigation-label">Administración & Contabilidad</div>
<ul class="pcoded-item pcoded-left-item">
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
<span class="pcoded-mtext">Tasa de Cambio</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('tasadecambio.index')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Ver</span>
<span class="pcoded-mcaret"></span>
</a>
<li class="">
<a href="{{route('tasadecambio.create')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Crear</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</li>
</ul>
</li>
	<!-- Inicio de Administración de Membresias -->
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
<span class="pcoded-mtext">Administración de Membresias</span>
<span class="pcoded-mcaret"></span>
</a>
	<!-- Membresias -->
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('membresia.index')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Ver</span>
<span class="pcoded-mcaret"></span>
</a>
<li class="">
<a href="{{route('membresia.create')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Crear</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('costoMembresia.index')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Resumen de Precios</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</li>
</ul>
</li>
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
<span class="pcoded-mtext">Servicios Urgentes</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('servicios.index')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Ver</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('servicios.create')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Crear</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>
</li>
</ul>
@endif