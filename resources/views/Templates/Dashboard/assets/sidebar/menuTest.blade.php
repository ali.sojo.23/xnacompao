@if(Auth::user()->role == 1000 || Auth::user()->role == 500)

<div class="pcoded-navigation-label">Mi Perfil (test)</div>
<ul class="pcoded-item pcoded-left-item">
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-user"></i><b>U</b></span>
<span class="pcoded-mtext">Mi Perfil</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('workExperienceEdit', ['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Nivel de estudios</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('docsUpdate',['id'=>encrypt(Auth::user()->id)])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Metodos de Pago</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>
</li>
</ul>
@include('Templates.Dashboard.assets.sidebar.menuContrato')
@endif