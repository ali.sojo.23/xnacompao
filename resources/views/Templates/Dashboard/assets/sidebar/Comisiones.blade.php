@if(Auth::user()->role != 1 || Auth::user()->role != 2)

<div class="pcoded-navigation-label">Comisiones</div>
<ul class="pcoded-item pcoded-left-item">
<li class="">
<a href="{{route('comisiones.show')}}">
<span class="pcoded-micon"><i class="ti-receipt"></i></span>
<span class="pcoded-mtext">Comisiones Individual</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('comisionesCall.show')}}">
<span class="pcoded-micon"><i class="ti-receipt"></i></span>
<span class="pcoded-mtext">Comisiones Call Center</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('comisionesReferidos.show')}}">
<span class="pcoded-micon"><i class="ti-receipt"></i></span>
<span class="pcoded-mtext">Comisiones Referidos</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>

@endif
