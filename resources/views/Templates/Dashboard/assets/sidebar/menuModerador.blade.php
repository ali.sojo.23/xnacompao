@if(Auth::user()->role == 1000 || Auth::user()->role == 500)
<div class="pcoded-navigation-label">Moderación de Usuarios</div>
<ul class="pcoded-item pcoded-left-item">
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-user"></i><b>U</b></span>
<span class="pcoded-mtext">Usuarios</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('moderations')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Pendientes por verificación datos</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('create')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Pendiente por completar catos de registro</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('verifications')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Pendiente por verificar correo</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>
</li>
</ul>
@endif
