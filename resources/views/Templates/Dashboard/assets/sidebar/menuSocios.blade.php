@if(Auth::user()->role == 1000 || Auth::user()->role == 700)

<div class="pcoded-navigation-label">Socio Activo</div>
<ul class="pcoded-item pcoded-left-item">
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-receipt"></i><b>U</b></span>
<span class="pcoded-mtext">Membresias</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('ventaMembresia')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Contratar Ahora</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('resumenVentas',['id'=>Auth::user()->id])}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Membresias vendidas</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>
</li>
<li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-bar-chart"></i><b>U</b></span>
<span class="pcoded-mtext">Ingresos</span>
<span class="pcoded-mcaret"></span>
</a>
<!-- <ul class="pcoded-submenu">
<li class="">
<a href="{{route('ventaMembresia')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Ver Ingresos</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul> -->
</li>
<!-- <li class="pcoded-hasmenu">
<a href="javascript:void(0)">
<span class="pcoded-micon"><i class="ti-receipt"></i><b>U</b></span>
<span class="pcoded-mtext">Servicios urgentes</span>
<span class="pcoded-mcaret"></span>
</a>
<ul class="pcoded-submenu">
<li class="">
<a href="{{route('invoice.index')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Mis Contratos</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="{{route('contrato.index')}}">
<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
<span class="pcoded-mtext">Contratar Ahora</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>
</li> -->
</ul>

@endif
