<nav class="navbar header-navbar pcoded-header">
<div class="navbar-wrapper">
<div class="navbar-logo">
<a class="mobile-menu" id="mobile-collapse" href="#!">
<i class="ti-menu"></i>
</a>
<div class="mobile-search">
<div class="header-search">
<div class="main-search morphsearch-search">
<div class="input-group">
<span class="input-group-addon search-close"><i class="ti-close"></i></span>
<input type="text" class="form-control" placeholder="Enter Keyword">
<span class="input-group-addon search-btn"><i class="ti-search"></i></span>
</div>
</div>
</div>	
</div>
<a href="/home">
<img class="img-fluid" src="{{asset('files/assets/images/logo.png')}}" alt="Theme-Logo" />
</a>
<a class="mobile-options">
<i class="ti-more"></i>
</a>
</div>
<div class="navbar-container container-fluid">
<ul class="nav-left">
<li>
<div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
</li>
<!-- Header Search
<li class="header-search">
<div class="main-search morphsearch-search">
<div class="input-group">
<span class="input-group-addon search-close"><i class="ti-close"></i></span>
<input type="text" class="form-control">
<span class="input-group-addon search-btn"><i class="ti-search"></i></span>
</div>
</div>
</li>
-->
<li>
<a href="#!" onclick="javascript:toggleFullScreen()">
<i class="ti-fullscreen"></i>
</a>
</li>
</ul>

<ul class="nav-right">
<!-- Table Notifications
@include('Templates.Dashboard.assets.header.notifications')
-->
<!-- Table Messages
<li class="">
<a href="#!" class="displayChatbox">
<i class="ti-comments"></i>
<span class="badge bg-c-green"></span>
</a>
</li>
-->
<!-- User Profile -->
@include('Templates.Dashboard.assets.header.users')
</ul>
</div>
</div>
</nav>