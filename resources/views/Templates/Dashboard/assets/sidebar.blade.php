<nav class="pcoded-navbar" pcoded-header-position="relative">
<div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
<div class="pcoded-inner-navbar main-menu">
<!-- Mi Perfil -->
@include('Templates.Dashboard.assets.sidebar.menuTest')

<!-- Perfil Cuidador -->
@include('Templates.Dashboard.assets.sidebar.menuTrabajador')

<!-- Perfil Cliente -->
@include('Templates.Dashboard.assets.sidebar.menuCliente')

<!-- Usuarios -->
@include('Templates.Dashboard.assets.sidebar.menuAdministrador')
@include('Templates.Dashboard.assets.sidebar.menuModerador')

<!-- Contabilidad -->
@include('Templates.Dashboard.assets.sidebar.menuContabilidad')

<!-- Socios -->
@include('Templates.Dashboard.assets.sidebar.menuSocios')

<!-- Atencion al cliente -->
@include('Templates.Dashboard.assets.sidebar.menuAtencionAlCliente')

<!-- Comisiones -->
@include('Templates.Dashboard.assets.sidebar.Comisiones') 




</div>
</nav>