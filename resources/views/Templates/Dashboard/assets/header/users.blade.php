<li class="user-profile header-notification">
<a href="#!">
@if(!Auth::guest())
	<?php if (!empty(Auth::user()->avatar)) { ?>

		<img src="{{asset('/users_images/'. Auth::user()->avatar)}}"class="img-radius" alt="User-Profile-Image">
	<?php }else{ ?>
		<img style="background: white;" src="{{asset('/users_images/default/cuidador/enfermero.png')}}"class="img-radius" alt="User-Profile-Image">
	<?php } ?>
<span>{{Auth::user()->name}} {{Auth::user()->lastName}}</span>
@endif
<i class="ti-angle-down"></i>
</a>
<ul class="show-notification profile-notification">
<!--<li>
<a href="#!">
<i class="ti-settings"></i> Configuracip
</a>
</li>-->
<li>
<a href="{{route('users.show',['user'=>encrypt(Auth::user()->id)])}}">
<i class="ti-user"></i> Mi Perfil
</a>
</li>
<li>
<a href="#" class="disabled">
<i class="ti-email"></i> Mis Mensajes
</a>
</li>
<li>
<a href="{{ route('logout') }}"
 	onclick="event.preventDefault();
 		document.getElementById('logout-form').submit();">
<i class="ti-layout-sidebar-left"></i> Cerrar Sesión
</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  {{ csrf_field() }}
</form>
</li>
</ul>
</li>