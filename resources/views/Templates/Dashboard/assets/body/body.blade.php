<div class="page-body">
<div class="row">
<div class="col-lg-12">
@if(Auth::user()->status == 2)
@if(empty(Auth::user()->id_docs))
<div class="alert alert-danger" role="alert">
  Para continuar con el proceso de verificación de su cuenta debe cargar sus archivos de identificación.
  <br>
  Puede hacerlo haciendo click <a href="{{route('docsUpdate',['id'=>Auth::user()->id])}}" title="">aquí</a>!.
</div>
@endif
@if(Auth::user()->role == 2)
<div class="alert alert-success" role="alert">
  <strong>Bienvenidos!!</strong> Gracias por registrar su datos en <a href="https://acompaño.com" title="acompaño.com">acompaño.com</a> está a solo un paso para poder contratar un acompañante <br>
  <strong>Su cuenta se encuentra en periodo de moderación</strong> una vez culminado este proceso dejará de ver este mensaje. <br><br>
  <strong>Disculpe las molestias ocasionadas pero lo hacemos por su seguridad y la de los suyos</strong>
</div>
@endif
@if(Auth::user()->role == 1)
<div class="alert alert-success" role="alert">
  <strong>Bienvenidos!!</strong> Gracias por registrar su datos en <a href="https://acompaño.com" title="acompaño.com">acompaño.com</a> está a solo un paso para poder comenzar a trabajar con nosotros <br><br>
  <strong>Su cuenta se encuentra en periodo de moderación</strong> una vez culminado este proceso dejará de ver este mensaje.
  <br>
  <strong>Disculpe las molestias ocasionadas pero lo hacemos por su seguridad</strong>
</div>
@endif
@endif
@if(Auth::user()->status == 1 && Auth::user()->role == 1)
<div class="alert alert-info" role="alert">
  <strong>Bienvenidos!!</strong> Gracias por registrarse en <a href="https://acompaño.com" title="acompaño.com">acompaño.com</a> está a solo un paso para poder comenzar a trabajar con nosotros <br>
  <strong>Complete el siguiente formulario</strong>
</div>
@endif
@if(Auth::user()->status == 1 && Auth::user()->role == 2)
<div class="alert alert-info" role="alert">
  <strong>Bienvenidos!!</strong> Gracias por registrarse en <a href="https://acompaño.com" title="acompaño.com">acompaño.com</a> está a solo un paso para poder contratar un acompañante médico<br>
  <strong>Complete el siguiente formulario</strong>
</div>
@endif


@yield('body')



</div>
</div>
</div>