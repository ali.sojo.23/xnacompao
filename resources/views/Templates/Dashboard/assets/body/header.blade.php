<div class="page-header card">
<div class="card-block">
<h5 class="m-b-10">Sidebar Fixed</h5>
<p class="text-muted m-b-10">lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
<ul class="breadcrumb-title b-t-default p-t-10">
<li class="breadcrumb-item">
<a href="index.html"> <i class="fa fa-home"></i> </a>
</li>
<li class="breadcrumb-item"><a href="#!">Page Layouts</a>
</li>
<li class="breadcrumb-item"><a href="#!">Vertical</a>
</li>
<li class="breadcrumb-item"><a href="#!">Sidebar Fixed</a>
</ul>
</div>
</div>