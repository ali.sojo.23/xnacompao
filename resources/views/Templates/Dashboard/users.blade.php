<!DOCTYPE html>
<html  lang="{{ app()->getLocale() }}">
<head>
<title>Bienvenido {{Auth::user()->name}} {{Auth::user()->lastName}} a acompaño.com | Dashboard </title>


<!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="Gradient Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
<meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
<meta name="author" content="codedthemes" />

<link rel="icon" href="{{asset('files/assets/images/favicon.png')}}" type="image/png">

<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/bootstrap/css/bootstrap.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/themify-icons/themify-icons.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/icofont/css/icofont.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/font-awesome/css/font-awesome.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/jquery.steps/css/jquery.steps.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('files/assets/css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('files/assets/css/jquery.mCustomScrollbar.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css">
</head>
<body>

<div class="theme-loader">
<div class="loader-track">
<div class="loader-bar"></div>
</div>
</div>

<div id="pcoded" class="pcoded">
<div class="pcoded-overlay-box"></div>
<div class="pcoded-container navbar-wrapper">
<!-- header -->
@include('Templates.Dashboard.assets.header')
<!-- sidebarchat -->
@include('Templates.Dashboard.assets.chat.sidebarChat')
<!-- Show Chat-->
@include('Templates.Dashboard.assets.chat.showChat')
<div class="pcoded-main-container">
<div class="pcoded-wrapper">
<!-- NavBAr -->
@include('Templates.Dashboard.assets.sidebar')
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<!-- Header Dashboard
@include('Templates.Dashboard.assets.body.header')
-->
<!-- Body Dashboard -->
@include('Templates.Dashboard.assets.body.body')
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>








<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="files/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="files/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="files/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="files/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="files/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->









<script src="{{asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
<script src="{{asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
<script src="{{asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

<script src="{{asset('files/bower_components/modernizr/js/modernizr.js')}}"></script>
<script src="{{asset('files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>

<script src="{{asset('files/bower_components/jquery.cookie/js/jquery.cookie.js')}}"></script>
<script src="{{asset('files/bower_components/jquery.steps/js/jquery.steps.js')}}"></script>
<script src="{{asset('files/bower_components/jquery-validation/js/jquery.validate.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="{{asset('files/assets/pages/form-validation/validate.js')}}"></script>

<script src="{{asset('files/assets/pages/forms-wizard-validation/form-wizard.js')}}"></script>
<script src="{{asset('files/assets/js/pcoded.min.js')}}"></script>
<script src="{{asset('files/assets/js/vertical/vertical-layout.min.js')}}"></script>
<script src="{{asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="{{asset('files/assets/js/script.js')}}"></script>
@include('Templates.Dashboard.assets.footer')
<script type="text/javascript">

$(document).ready(function() {
    $('#selectServicio').select2({
        theme: "bootstrap"
    });
    $('#selectDepartamento').select2({
        theme: "bootstrap"
    });
    $('#selectPais').select2({
        theme: "bootstrap"
    });
    $('#selectPais1').select2({
        theme: "bootstrap"
    });
    $('#selectPais2').select2({
        theme: "bootstrap"
    });
});
    
</script>
<script language="javascript">
  function sumar()
  {
      var total = 0;
      var valor1 = document.getElementById("val1")
      var valor2 = document.getElementById("val2")

  total.value = parseInt(val1.value) * parseInt(val2.value);

  var Display = document.getElementById("Display");
  Display.innerHTML = total;
  }
  </script>
</body>
</html>