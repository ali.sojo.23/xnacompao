<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"><![endif]-->
<!--[if gt IE 9 | !IE]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ACOMPAÑO</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="icon" href="assets/img/favicon.png" type="image/png">
  <link rel='stylesheet' href='assets/css/bootstrap.min.css'>     <!-- bootstrap -->
  <link rel='stylesheet' href='assets/css/vendor.css'>            <!-- plugin -->
  <link rel='stylesheet' href='assets/css/yellow.css'> <!-- edit to be red.css, orange.css, pink.css, yellow.css , purple.css, blue.css , brown.css, green.css -->
  <link rel='stylesheet' href='assets/css/demo.css'>              <!-- demo style -->
  <link rel='stylesheet' href='assets/css/custom.css'>            <!-- custom style should place in this file -->
  <link rel="stylesheet" href="assets/css/ionicons.min.css">
  <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
  <!--[if lte IE 9]><!-->
  <script src='assets/js/vendor/html5shiv.min.js'></script>
  <!--<![endif]-->
</head>

<body>

 <div class="site-loader">
    <span></span>
    <p>ACOMPAÑO</p>
 </div> <!-- .site-loader -->

    

  <div id="siteBg" class="site-bg">
    <div class="site-bg__img"></div>
    <div class="site-bg__video"></div>
    <div class="site-bg__overlay"></div>
    <div class="site-bg__effect"></div>
  </div> <!-- .site-bg -->

   <div class="site-wrap">
    <header class="site-header">
       <a style="position: absolute; right:0" class="btn btn-success" href="/login" title="">Iniciar Sesión</a>
    </header> <!-- .site-header -->

    <main class="site-main">

      <div id="home" class="section is-active">
        <div class="section__table">
          <div class="section__cell">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                  <div class="overflow-hidden">
                    <div class="animation-item"
                    data-animation-in="fadeInUp2"
                    data-animation-in-delay="300"
                    data-animation-out="fadeOutDown2"
                    data-animation-out-delay="700">
                      <p class="section__caption">¡Estamos en construcción!</p>
                    </div>
                  </div>
                  <div class="overflow-hidden">
                    <div class="animation-item"
                    data-animation-in="fadeInDown"
                    data-animation-in-delay="900"
                    data-animation-out="fadeOutDown"
                    data-animation-out-delay="300">

                      <div class="row" id="countdown_dashboard">
                        <div class="col-xs-3 dash days_dash">
                          
                          <div class="digit">0</div>
                          <div class="digit">0</div>
                          <span class="dash_title">days</span>
                        </div>

                        <div class="col-xs-3 dash hours_dash">
                          <div class="digit">0</div>
                          <div class="digit">0</div>
                          <span class="dash_title">hours</span>
                        </div>

                        <div class="col-xs-3 dash minutes_dash">
                          <div class="digit">0</div>
                          <div class="digit">0</div>
                          <span class="dash_title">minutes</span>
                        </div>

                        <div class="col-xs-3 dash seconds_dash">
                          <div class="digit">0</div>
                          <div class="digit">0</div>
                          <span class="dash_title">seconds</span>
                        </div>
                      </div> <!-- #countdown_dashboard -->
                    </div>
                  </div>
                  <div class="overflow-hidden">
                    <div class="animation-item"
                    data-animation-in="fadeInUp2"
                    data-animation-in-delay="1100"
                    data-animation-out="fadeOutDown2"
                    data-animation-out-delay="100">
                    <br>
                       <a type="button" class="btn btn-warning" title="registrate" href="/register">Registrate</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        
      </div> <!-- #home -->

     
    </main> <!-- .site-main -->

    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="site-footer__inner">
              <p class="site-footer__copyright">
                © 2018 Acompaño.com All Right reserved, Design by Grupo AKWEB C.A.
              </p>

              
            </div>
          </div>
        </div>
      </div>
    </footer> <!-- .site-footer -->
  </div>
  <!-- /site wrap -->

  <!-- script -->
  <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
  <script src='assets/js/vendor/bootstrap.min.js'></script>
  <script src='assets/js/vendor/plugin.js'></script>
  <script src='assets/js/variable.js'></script>
  <script src='assets/js/main.js'></script>
  <script src='assets/js/demo.js'></script>
  @include('Templates.Dashboard.assets.footer')
  <!-- /script -->

</body>
</html>