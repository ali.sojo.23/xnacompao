<!DOCTYPE html>
<html  lang="{{ app()->getLocale() }}">
<head>
<title>Bienvenido {{Auth::user()->name}} {{Auth::user()->lastName}} a acompaño.com | Dashboard </title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Favicon icon -->

    <link rel="icon" href="{{asset('files/assets/images/favicon.png')}}" type="image/png">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css')}}">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css" ')}}/>
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datedropper/css/datedropper.min.css" ')}}/>
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/themify-icons/themify-icons.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/icofont/css/icofont.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/font-awesome/css/font-awesome.min.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/css/jquery.mCustomScrollbar.css')}}">
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
<div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
<!-- header -->
@include('Templates.Dashboard.assets.header')
<!-- sidebarchat -->
@include('Templates.Dashboard.assets.chat.sidebarChat')
<!-- Show Chat-->
@include('Templates.Dashboard.assets.chat.showChat')
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">

@include('Templates.Dashboard.assets.sidebar')                    
<div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    
                                    <!-- Page-header end -->

                                    <!-- Page-body start -->
<div class="page-body">
<!--profile cover start-->
<div class="row">
<div class="col-lg-12">
<div class="cover-profile">
<div class="profile-bg-img">
<img class="profile-bg-img img-fluid" src="http://html.codedthemes.com/gradient-able/files/assets/images/user-profile/bg-img1.jpg" alt="bg-img">

<div class="card-block user-info">

<div class="col-md-12">

<div class="media-left row">
<div class="col-md-2">
<a href="#" class="profile-image">
<img style="height: 10rem; background-color: white" class="user-img img-radius img-responsive" src="https://www.acompaño.com/users_images/<?php if(!empty($user->avatar)) : echo $user->avatar; else: echo "default/cuidador/enfermero.png"; endif;  ?>" alt="user-img">
</a>
</div>
</div>

<div class="media-body row">

<div class="col-lg-12">

<div class="user-title">

<h2>{{$user->name}} {{$user->lastName}}</h2>

<span class="text-white"></span>

</div>

</div>

<div>

<div class="pull-right cover-btn">
<!-- <button type="button" class="btn btn-primary m-r-10 m-b-5"><i class="icofont icofont-plus"></i> Follow
</button>
<button type="button" class="btn btn-primary"><i class="icofont icofont-ui-messaging"></i> Message</button> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--profile cover end-->
<div class="row">
<div class="col-lg-12">
<!-- tab header start -->
<div class="tab-header card">
<ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Información Personal</a>
        <div class="slide"></div>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">Servicios</a>
        <div class="slide"></div>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#review" role="tab">Comentarios de clientes</a>
        <div class="slide"></div>
    </li>
</ul>
</div>
<!-- tab header end -->
<!-- tab content start -->
<div class="tab-content">
<!-- tab panel personal start -->
<div class="tab-pane active" id="personal" role="tabpanel">
<!-- personal card start -->
<div class="card">
<div class="card-header">
<h5 class="card-header-text">Información personal</h5>
<!-- <button id="edit-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right">
<i class="icofont icofont-edit"></i>
</button> -->
</div>
<div class="card-block">
<div class="view-info">
<div class="row">
<div class="col-lg-12">
 <div class="general-info">
<div class="row">
<div class="col-lg-12 col-xl-6">
<div class="table-responsive">
<table class="table m-0">
<tbody>
<tr>
<th scope="row">Nombre Completo</th>
<td>{{$user->name}} {{$user->lastName}}</td>
</tr>
<tr>
<th scope="row">Genero</th>
<td>
    <?php if($user->sexo == 1) : ?>
        Masculino
    <?php else : ?>
        Femenino
    <?php endif ?>
</td>
</tr>
<tr>
<th scope="row">Fecha de Nacimeinto</th>
<td>{{ date('d-M-Y',strtotime($user->BirthDay))}}</td>
</tr>

</tbody>
</table>
</div>
</div>
<!-- end of table col-lg-6 -->
<div class="col-lg-12 col-xl-6">
<div class="table-responsive">
<table class="table">
<tbody>
<tr>
<th scope="row">Email</th>
<td><a href="#!">{{$user->email}}</a></td>
</tr>
<tr>
<th scope="row">Ubicación</th>
<td>@if(!empty($user->estados->CiudadNombre)){{$user->estados->CiudadNombre}}, {{$user->country}} @endif</td>
</tr>
</tbody>
</table>
</div>
</div>
<!-- end of table col-lg-6 -->
</div>
<!-- end of row -->
</div>
<!-- end of general info -->
</div>
<!-- end of col-lg-12 -->
</div>
<!-- end of row -->
</div>
<!-- end of view-info -->
<form action="" method="POST">

<div class="edit-info">
<div class="row">
<div class="col-lg-12">
<div class="general-info">
<div class="row">
<div class="col-lg-6">
<table class="table">
<tbody>
<tr>
<td>
<div class="input-group">
<span class="input-group-addon"><i class="icofont icofont-user"></i></span>
<input type="text" class="form-control" placeholder="Full Name">
</div>
</td>
</tr>
<tr>    
<td>
<div class="form-radio">
<div class="group-add-on">
<div class="radio radiofill radio-inline">
<label>
<input type="radio" name="radio" checked><i class="helper"></i> Male
</label>
</div>
<div class="radio radiofill radio-inline">
<label>
<input type="radio" name="radio"><i class="helper"></i> Female
</label>
</div>
</div>
</div>
</td>
</tr>
<tr>
<td>
<input id="dropper-default" class="form-control" type="text" placeholder="Select Your Birth Date" />
</td>
</tr>
<tr>
<td>
<select id="hello-single" class="form-control">
<option value="">---- Marital Status ----</option>
<option value="married">Married</option>
<option value="unmarried">Unmarried</option>
</select>
</td>
</tr>
<tr>
<td>
<div class="input-group">
<span class="input-group-addon"><i class="icofont icofont-location-pin"></i></span>
<input type="text" class="form-control" placeholder="Address">
</div>
</td>
</tr>
</tbody>
</table>
</div>
<!-- end of table col-lg-6 -->
<div class="col-lg-6">
<table class="table">
<tbody>
<tr>
<td>
<div class="input-group">
<span class="input-group-addon"><i class="icofont icofont-mobile-phone"></i></span>
<input type="text" class="form-control" placeholder="Mobile Number">
</div>
</td>
</tr>
<tr>
<td>
<div class="input-group">
<span class="input-group-addon"><i class="icofont icofont-social-twitter"></i></span>
<input type="text" class="form-control" placeholder="Twitter Id">
</div>
</td>
</tr>
<tr>
<td>
<div class="input-group">
<span class="input-group-addon"><i class="icofont icofont-social-skype"></i></span>
<input type="email" class="form-control" placeholder="Skype Id">
</div>
</td>
</tr>
<tr>
<td>
<div class="input-group">
<span class="input-group-addon"><i class="icofont icofont-earth"></i></span>
<input type="text" class="form-control" placeholder="website">
</div>
</td>
</tr>
</tbody>
</table>
</div>
<!-- end of table col-lg-6 -->
</div>
<!-- end of row -->
<div class="text-center">
<a type="submit" href="#!" class="btn btn-primary waves-effect waves-light m-r-20">Guardar</a>
<a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancelar</a>
</div>
</div>
<!-- end of general info -->
</div>
<!-- end of col-lg-12 -->
</div>
<!-- end of row -->
</div>
</form>
<!-- end of edit-info -->
</div>
<!-- end of card-block -->
</div>
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<h5 class="card-header-text">Descripción Profesional</h5>
<!-- <button id="edit-info-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right">
<i class="icofont icofont-edit"></i>
</button> -->
</div>
<div class="card-block user-desc">
<div class="edit-desc">
<form action="" method="POST">
<div class="col-md-12">
<input type="hidden" name="users_id" value="{{$user->id}}">
<textarea class="form-control" rows="10" name="descripcion">

</textarea>
</div>
<div class="text-center">
<button type="submit" class="btn btn-primary waves-effect waves-light m-r-20 m-t-20">Guardar Descripción</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<!-- personal card end-->
</div>
                                                    <!-- tab pane personal end -->
                                                    <!-- tab pane info start -->
<div class="tab-pane" id="binfo" role="tabpanel">
                                                        <!-- info card start -->
<div class="card">
<div class="card-header">
<h5 class="card-header-text">Servicios que ofrece el acompañante</h5>
</div>

<div class="card-block">
<div class="row">
@if(!empty($user->servicio->sanatorios))
<div class="col-md-6">
<div class="card b-l-success business-info services m-b-20">
<div class="card-header">
<div class="service-header">
<a href="#">
<h5 class="card-header-text">Sanatorio</h5>
</a>
</div>
</div>
<!-- end of card-block -->
</div>
</div>
@endif
@if(!empty($user->servicio->domicilios))
<div class="col-md-6">
<div class="card b-l-danger business-info services">
<div class="card-header">
<div class="service-header">
<a href="#">
<h5 class="card-header-text">Domicilios</h5>
</a>
</div>

</div>
<!-- end of card-block -->
</div>
</div>
@endif
@if(!empty($user->servicio->hospitales))
<div class="col-md-6">
<div class="card b-l-warning business-info services">
<div class="card-header">
<div class="service-header">
<a href="#">
<h5 class="card-header-text">Hospitales</h5>
</a>
</div>
</div>
<!-- end of card-block -->
</div>
</div>
@endif
<!-- fin de bloque -->
</div>
</div>
</div>
                                                    
<!-- info card end -->
</div>
<!-- tab pane info end -->
<div class="tab-pane" id="review" role="tabpanel">
<div class="card">
<div class="card-header">
<h5 class="card-header-text">Comentarios</h5>
</div>
<div class="card-block">
<ul class="media-list">
<!-- <li class="media">
<div class="media-left">
<a href="#">
<img class="media-object img-radius comment-img" src="files/assets/images/avatar-1.jpg" alt="Generic placeholder image">
</a>
</div>
<div class="media-body">
<h6 class="media-heading">Sortino media<span class="f-12 text-muted m-l-5">Just now</span></h6>
<div class="stars-example-css review-star">
<i class="icofont icofont-star"></i>
<i class="icofont icofont-star"></i>
<i class="icofont icofont-star"></i>
<i class="icofont icofont-star"></i>
<i class="icofont icofont-star"></i>
</div>
<p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
<div class="m-b-25">
<span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
</div>
<hr>
</div>
</li> -->

</ul>
<div class="input-group">
<input type="text" class="form-control" placeholder="Right addon" disabled="">
<span class="input-group-addon" disabled><i class="icofont icofont-send-mail"></i></span>
</div>

</div>
</div>
</div>
<!-- tab content end -->
</div>
</div>
</div>
<!-- Page-body end -->
</div>
</div>
<!-- Main body end -->

</div>
</div>
</div>
</div>
</div>
</div>



    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="files/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="files/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="files/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="files/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="files/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script  src="{{asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
    <script  src="{{asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
    <script  src="{{asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
    <script  src="{{asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- jquery slimscroll js -->
    <script  src="{{asset('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
    <!-- modernizr js -->
    <script  src="{{asset('files/bower_components/modernizr/js/modernizr.js')}}"></script>
    <script  src="{{asset('files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>

    <!-- Bootstrap date-time-picker js -->
    <script  src="{{asset('files/assets/pages/advance-elements/moment-with-locales.min.js')}}"></script>
    <script  src="{{asset('files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script  src="{{asset('files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js')}}"></script>
    <!-- Date-range picker js -->
    <script  src="{{asset('files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js')}}"></script>
    <!-- Date-dropper js -->
    <script  src="{{asset('files/bower_components/datedropper/js/datedropper.min.js')}}"></script>
    <!-- data-table js -->
    <script src="{{asset('files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
    <!-- ck editor -->
    <!-- <script src="files/assets/pages/ckeditor/ckeditor.js"></script> -->
    <!-- echart js -->
    <script src="{{asset('files/assets/pages/chart/echarts/js/echarts-all.js')}}" ></script>
    <script src="{{asset('files/assets/pages/user-profile.js')}}"></script>
    <script src="{{asset('files/assets/js/pcoded.min.js')}}"></script>
    <script src="{{asset('files/assets/js/vertical/vertical-layout.min.js')}}"></script>
    <script src="{{asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- Custom js -->
<script  src="{{asset('files/assets/js/script.js')}}"></script>

</body>

</html>
