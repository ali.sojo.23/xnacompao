@extends('Templates.Dashboard.index')
@section('body')
<!-- Información -->
<div class="row">

<div class="col-lg-12">

<div class="job-card card-columns"> 
<div class="col-sm-12">
<div class="card card-border-primary">
<div class="card-header">
<h3></h3>
<!-- <span class="label label-default f-right"> 28 January, 2015 </span> -->
</div>
<div class="card-block">
<div class="row">

<div class="col-sm-12">
<h1 class="text-center text-danger" style="font-size: 7rem"><i class="fa fa-user-times" aria-hidden="true"></i></h1>
</div>
</div>
<div class="row">

	<div class="col-sm-12">
		<h3 class="text-center">Usuario No Registrado</h3>
	</div>
</div>
</div>
<div class="card-footer">
<div class="task-board m-0">
<a href="{{ route('users.create')}}" class="btn btn-info b-none">Registrar usuario</a>

<!-- end of dropdown-secondary -->
</div>
</div>
</div>
</div>

<!-- Segundo Recuadro -->
<div class="col-sm-12">
<div class="card card-border-primary">
<div class="card-header">
<h3></h3>
<!-- <span class="label label-default f-right"> 28 January, 2015 </span> -->
</div>
<div class="card-block">
<div class="row">

<div class="col-sm-12">
<h1 class="text-center text-success" style="font-size: 7rem"><i class="fa fa-user-plus" aria-hidden="true"></i></h1>
</div>
</div>
<div class="row">

	<div class="col-sm-12">
		<h3 class="text-center">Usuario Registrado</h3>
	</div>
</div>
</div>
<div class="card-footer">
<div class="task-board m-0">
<a href="{{ route('ventaMembresiaUsuario')}}" class="btn btn-info b-none">
Proceder con el contrato</a>
<!-- end of dropdown-secondary -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>


 @stop