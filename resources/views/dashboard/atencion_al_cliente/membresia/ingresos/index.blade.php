@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Usuarios Registrados</h5>
<span>En la siguiente lista puede observar las personas registradas en sitio web, puede usar el buscador para filtrar por: Nombre, Apellido, Rol y País de residencia.</span>
</div>
<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th># Factura</th>
<th>Membresia</th>
<th>Tipo</th>
<th>Total de la Factura</th>
<th>Fecha de La Factura</th>
<th>Promedio de beneficico</th>
</tr>
</thead>
<tbody>

@foreach($facturas as $factura)
@if($factura[0]->estado == 2)
<tr>
<td><?php echo $factura[0]->id; ?></td>
<td><?php echo $factura[0]->membresia->costo->membresia->membresia . " " . $factura[0]->membresia->costo->hora ?></td>
<td><?php if($factura[0]->membresia->costo->membresia->profesional == 1): echo "profesional"; else : echo "no profesional"; endif; ?></td>
<td><?php echo $factura[0]->total . " ". $factura[0]->monedaLocal ?></td>
<td><?php echo date('M d, Y',strtotime($factura[0]->created_at)) ?></td>
<td><?php echo $factura[0]->total*0.20 . " ". $factura[0]->monedaLocal ?></td>
<td>
</td>
</tr>
@endif
@endforeach
</tbody>
<tfoot>
<tr>
<th># Factura</th>
<th>Membresia</th>
<th>Tipo</th>
<th>Total de la Factura</th>
<th>Fecha de la factura</th>
<th>Promedio de beneficico</th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop