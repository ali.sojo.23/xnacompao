@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h5>¡Contrata un Cuidador ahora!</h5>
<span>Seleccione los campos necesarios para proceder con la contratación. <br>
recuerde que si usted requiere de mas de 6 días continuos debe contratatar a 2 personas distintas</span>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="GET" action="{{route('ventaMembresiaPais')}}" enctype="multipart/form-data">
{{ csrf_field() }}
<h3> Contratación </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="email-2" class="block">Tipo de Membresia</label>
</div>
<div class="col-md-8 col-lg-10">
	<select class="form-control" name="membresia">
			<option>Seleccione el tipo de Membresias</option>
			<option value="1">Membresia Profesional</option>
			<option value="0">Membresia NO Profesional</option>
	</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="email-2" class="block">Seleccione el Pais a contratar</label>
</div>
<div class="col-md-8 col-lg-10">
<select class="form-control" name="pais">
	<option>Seleccione el Pais</option>
<?php foreach ($pais as $pais) { ?>
	<option value="{{$pais->id}}"> {{$pais->pais->PaisNombre}}</option>
<?php } ?>
</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="confirm-2" class="block">¿Usuario Existente?</label>
</div>
<div class="col-md-8 col-lg-10">
	<select class="form-control" name="usuario">
			<option>---- Selecciona un cliente en la lista desplegable ---- </option>
		@foreach($usuarios as $usuario)
			@if($usuario->role == 2)
				<option value="{{$usuario->id}}">{{$usuario->name}} {{$usuario->lastName}}</option>
			@endif
		@endforeach
	</select>
</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>


@stop