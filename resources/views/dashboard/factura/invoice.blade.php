<!DOCTYPE html>
<html  lang="{{ app()->getLocale() }}">
<head>
<title>Bienvenido {{Auth::user()->name}} {{Auth::user()->lastName}} a acompaño.com | Dashboard  | Factura # ACOMPA- {{$contrato->factura->id}} </title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('files/assets/images/favicon.png')}}" type="image/png">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/themify-icons/themify-icons.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/icofont/css/icofont.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/icon/font-awesome/css/font-awesome.min.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/css/jquery.mCustomScrollbar.css')}}">
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <!-- header -->
@include('Templates.Dashboard.assets.header')
<!-- sidebarchat -->
@include('Templates.Dashboard.assets.chat.sidebarChat')
<!-- Show Chat-->
@include('Templates.Dashboard.assets.chat.showChat')
            <!-- Sidebar inner chat end-->
<div class="pcoded-main-container">

<div class="pcoded-wrapper">
@include('Templates.Dashboard.assets.sidebar')

<div class="pcoded-content">
<div class="pcoded-inner-content">
<!-- Main-body start -->
<div class="main-body">
<div class="page-wrapper">
<!-- Page body start -->
<div class="page-body">
<!-- Container-fluid starts -->
<div class="container">
<!-- Main content starts -->
<div>
<!-- Invoice card start -->
<div class="card">
<div class="row invoice-contact">
<div class="col-md-8">
<div class="invoice-box row">
<div class="col-sm-12">
<table class="table table-responsive invoice-table table-borderless">
<tbody>
<tr>
<td><img src="{{asset('files/assets/images/logo.png')}}" class="m-b-10" alt=""></td>
</tr>
<tr>
<td>ACOMPAÑO.com</td>
</tr>
<tr>
<td>dirección</td>
</tr>
<tr>
<td><a href="mailto:demo@gmail.com" target="_top">contacto@acompaño.com</a>
</td>
</tr>
<tr>
<td>+91 919-91-91-919</td>
</tr>
<tr>
<td><a href="https://acompaño.com" target="_blank">www.acompaño.com</a>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
<div class="col-md-4">
</div>
</div>
<div class="card-block">
<div class="row invoive-info">
<div class="col-md-4 col-xs-12 invoice-client-info">
<h6>Información del Cliente:</h6>
<h6 class="m-0">{{$user->name}} {{$user->lastName}}</h6>
<p class="m-0 m-t-10">{{$pais[0]->PaisNombre}}, {{$ciudad[0]->CiudadNombre}}</p>
<p class="m-0">{{$user->phone}}1</p>
<p>{{$user->email}}</p>
</div>
<div class="col-md-4 col-sm-6">
<h6>Order Information :</h6>
<table class="table table-responsive invoice-table invoice-order table-borderless">
<tbody>
<tr>
<th>Fecha :</th>
<td><?php echo date('M d, Y',strtotime($factura[0]->created_at)) ?></td>
</tr>
<tr>
<th>Estado de pago :</th>
<td>
<?php if($factura[0]->estado == 1) : ?>
<span class="label label-danger">Pendiente</span>
<?php else : ?>
<span class="label label-success">Pagada</span>
<?php endif ?>   
</td>
</tr>
<tr>
<th>Id de contrato :</th>
<td>
{{$contrato->id}}
</td>
</tr>
</tbody>
</table>
</div>
<div class="col-md-4 col-sm-6">
<h6 class="m-b-20">Invoice Number <span>ACOMPA-{{$factura[0]->id}}</span></h6>
<h6 class="text-uppercase text-primary">Total a Pagar :
<span>{{$factura[0]->total}} {{$factura[0]->monedaLocal}}</span>
</h6>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<div class="table-responsive">
<table class="table  invoice-detail-table">
<thead>
<tr class="thead-default">
<th>Descripción</th>
<th>Horas</th>
<th>Precio Unitario</th>
<th>Total</th>
</tr>
</thead>
<tbody>
@foreach($cuidadores as $cuidador)
<tr>
<td>
<h6><?php foreach ($usuarios  as $usuario) {
    if($cuidador == $usuario->id){ echo $usuario->name . " " . $usuario->lastName;}
} ?>
    
</h6>
<p>
    <?php 
        if($contrato->servicio == 1){ $servicio = 'Domicilios'; }
        elseif($contrato->servicio == 2){ $servicio = 'Hospitales'; }
        elseif($contrato->servicio == 3){ $servicio = 'Sanatorios'; }

    $description =  '<strong>Servicios en ' . 
                    $servicio.
                    '</strong> desde el ' .  
                    date('M d, Y',strtotime($contrato->fechaInicio)) .
                    ' hasta ' .
                    date('M d, Y',strtotime($contrato->fechaFin)); ?> 
    <?php echo $description; ?>
</p>
</td>
<td>
<?php 
$dia1 = new dateTime($contrato->fechaInicio);
$dia2 = new dateTime($contrato->fechaFin);
$dias = $dia2->diff($dia1);
echo $dias = $dias->d+1 *8;

 ?>
</td>
<td><?php echo $factura[0]->subTotal/$dias . " ". $factura[0]->monedaLocal ?></td>
<td><?php echo $factura[0]->subTotal . " ". $factura[0]->monedaLocal ?></td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<table class="table table-responsive invoice-table invoice-total">
<tbody>
<tr>
<th>Sub Total :</th>
<td><?php echo $factura[0]->subTotal . " ". $factura[0]->monedaLocal ?></td>
</tr>
<tr>
<th>Taxes (16%) :</th>
<td><?php echo $factura[0]->tax . " ". $factura[0]->monedaLocal ?></td>
</tr>
<tr>
<th>Discount (5%) :</th>
<td><?php echo $factura[0]->descuento . " ". $factura[0]->monedaLocal ?></td>
</tr>
<tr class="text-info">
<td>
<hr/>
<h5 class="text-primary">Total :</h5>
</td>
<td>
<hr/>
<h5 class="text-primary"><?php echo $factura[0]->total . " ". $factura[0]->monedaLocal ?></h5>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<h6>Terminos y Condiciones :</h6>
<p>Todos los terminos y condiciones que aplican a la siguiente facturación puede verla a travéz del siguiente link <a style="color:blue" href="#">Ver Terminos y Condiciones</a></p>
</div>
</div>
</div>
</div>
<!-- Invoice card end -->
<div class="row text-center">
<div class="col-sm-12 invoice-btn-group text-center">
<a onclick="window.history.back();" style="color:white" class="btn btn-danger waves-effect m-b-10 btn-sm waves-light">Atras</a>
<!-- Boton Mercado Pago -->
@if($factura[0]->estado == 1)
<form action="{{route('recibirPagos')}}" method="POST">
{{ csrf_field() }}
<input type="hidden" name="pais" value="<?php echo $pais[0]->PaisCodigo ?>">
<input type="hidden" name="factura" value="<?php echo $factura[0]->id ?>">
<input type="hidden" name="description" value="<?php echo $description ?>">
<input type="hidden" name="user_id" value="<?php echo Auth::user()->id ?>">
    <!-- condicional según pais de disponibilidad de mercadoPago -->
    <?php if ($pais[0]->PaisCodigo == 'ARG'): ?>
        <?php echo "<script
        src='https://www.mercadopago.com.ve/integrations/v1/web-tokenize-checkout.js' "; ?>
        <?php echo "data-public-key='" . env('MERCADOPAGO_PUBLIC_KEY_UY') . "'" ; ?>
        <?php echo "data-transaction-amount=" . $factura[0]->total;?> 
    <?php elseif($pais[0]->PaisCodigo == 'VEN'): ?>
        <?php echo "<script
        src='https://www.mercadopago.com.co/integrations/v1/web-tokenize-checkout.js' "; ?>
        <?php echo "data-public-key='" . env('MERCADOPAGO_PUBLIC_KEY_UY') . "'" ; ?>
        <?php echo "data-transaction-amount=" . $factura[0]->total;?>
    <?php endif ?>
    <?php echo ">" ?>
  </script>
</form>
@endif
<!-- Fin del boton de pago de mercado pago -->
</div>
</div>
</div>
</div>
<!-- Container ends -->
</div>
<!-- Page body end -->
</div>
</div>
<!-- Warning Section Starts -->

 </div>
 </div>
 </div>
 </div>
 </div>
 </div>



    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
        <div class="iew-container">
            <ul class="iew-download">
                <li>
                    <a href="http://www.google.com/chrome/">
                        <img src="../files/assets/images/browser/chrome.png" alt="Chrome">
                        <div>Chrome</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                        <img src="../files/assets/images/browser/firefox.png" alt="Firefox">
                        <div>Firefox</div>
                    </a>
                </li>
                <li>
                    <a href="http://www.opera.com">
                        <img src="../files/assets/images/browser/opera.png" alt="Opera">
                        <div>Opera</div>
                    </a>
                </li>
                <li>
                    <a href="https://www.apple.com/safari/">
                        <img src="../files/assets/images/browser/safari.png" alt="Safari">
                        <div>Safari</div>
                    </a>
                </li>
                <li>
                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="../files/assets/images/browser/ie.png" alt="">
                        <div>IE (9 & above)</div>
                    </a>
                </li>
            </ul>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
    <![endif]-->
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script  src="{{asset('files/bower_components/jquery/js/jquery.min.js')}}"></script>
    <script  src="{{asset('files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
    <script  src="{{asset('files/bower_components/popper.js/js/popper.min.js')}}"></script>
    <script  src="{{asset('files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- jquery slimscroll js -->
    <script  src="{{asset('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
    <!-- modernizr js -->
    <script  src="{{asset('files/bower_components/modernizr/js/modernizr.js')}}"></script>
    <script  src="{{asset('files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>
    <!-- Custom js -->
    <script src="{{asset('files/assets/js/pcoded.min.js')}}"></script>
    <script src="{{asset('files/assets/js/vertical/vertical-layout.min.js')}}"></script>
    <script src="{{asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script  src="{{asset('files/assets/js/script.js')}}"></script>
    
</body>

</html>
