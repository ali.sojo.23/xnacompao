
@extends('Templates.Dashboard.users')

@section('body')
<div class="card">
<div class="card-header">
<h5>Disponibilidad de horarios</h5>
<span>Seleccione su disponibilidad de horario y días de la semana que estará disponible.</span>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('horariosDomicilioUpdate',['id'=>encrypt(Auth::user()->id)])}}" enctype="multipart/form-data">
{{ method_field('PUT')}}
{{ csrf_field() }}
<h3> Domicilio</h3>
<fieldset>
<h4>Meses</h4>
<br>
<div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Enero</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="enero" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Febrero</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="febrero" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Marzo</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="marzo" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Abril</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="abril" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Mayo</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="mayo" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Junio</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="junio" value="1">
</div>
</div>
<div class="row">
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Julio</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="julio" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Agosto</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="agosto" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Septiembre</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="septiembre" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Octubre</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="octubre" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Noviembre</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="noviembre" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Diciembre</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="diciembre" value="1">
</div>
</div>	
</div>
<h4>Horas de trabajos diarios</h4>
<div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">00:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="0horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">01:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="1horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">02:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="2horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">03:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="3horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">04:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="4horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">05:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="5horas" value="1">
</div>
</div>
<div class="row">
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">06:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="6horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">7:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="7horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">08:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="8horas" value="1">
</div>

<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">09:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="9horas" value="1">
</div>

<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">10:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="10horas" value="1">
</div>

<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">11:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="11horas" value="1">
</div>
	
</div>
<div class="row">
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">12:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="12horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">13:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="13horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">14:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="14horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">15:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="15horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">16:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="16horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">17:00h</label>
</div>
<div class="col-md-8 col-lg-1">
<input type="checkbox" name="17horas" value="1">
</div>
</div>
<div class="row">
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">18:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="18horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">19:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="19horas" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">20:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="20horas" value="1">
</div>

<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">21:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="21horas" value="1">
</div>

<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">22:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="22horas" value="1">
</div>

<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">23:00h</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="23horas" value="1">
</div>

</div>
</div>
<h4>Días Disponibles</h4>
<br>
<div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">lunes</label>
</div>
<div class="col-md-8 col-lg-1">
	<input class="form-control" type="checkbox" name="lunes" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">martes</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="martes" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">miercoles</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="miercoles" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">jueves</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="jueves" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">viernes</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="viernes" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">sabado</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="sabado" value="1">
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">domingo</label>
</div>
<div class="col-md-8 col-lg-1">
	<input type="checkbox" name="domingo" value="1">
</div>
</div>
</div>
</fieldset>

</form>
</section>
</div>
</div>
</div>
</div>
</div>


@stop