@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Usuarios registrados pendientes por Moderación</h5>
<span>En la siguiente lista puede observar las personas registradas en sitio web, puede usar el buscador para filtrar por: Nombre, Apellido, Rol y País de residencia.</span>
</div>
<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>#</th>
<th>Nombre</th>
<th>apellido</th>
<th>Documentos</th>
<th>Correo</th>
<th>Rol</th>
<th>País</th>
<th></th>
</tr>
</thead>
<tbody>
<?php $id = 1; ?>
@foreach($users as $user)
@if($user->status == 2)
@if($user->role == 1 || $user->role == 2)
<tr>
<td>{{ $id++ }}</td>
<td>{{$user->name}}</td>
<td>{{$user->lastName}}</td>
<td>
	@if(empty($user->id_docs))
	Pendientes
	@else
	Cargados
	@endif
</td>
<td>{{$user->email}}</td>
<td>
@if($user->role == 1)
	Cuidador
@endif
@if($user->role == 2)
	Cliente
@endif
</td>
<td>{{$user->pais->PaisNombre}}</td>
<td>
	<a class="text-info" href="{{route('users.show',['user'=>encrypt($user->id)])}}"> Ver perfil</a><br>
	<a class="text-warning" href="{{ route('endModerate',['id'=>$user->id]) }}"
 	onclick="event.preventDefault();
 		document.getElementById('Moderar').submit();">
Habilitar
</a></td>
<form id="Moderar" action="{{ route('endModerate',['id'=>$user->id]) }}" method="POST" enctype="multipart/form-data" style="display: none;">
	{{ method_field('PUT')}}
  	{{ csrf_field() }}
  	<input type="hidden" name="status" value="3">
</form>
</tr>
@endif
@endif
@endforeach
</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Nombre</th>
<th>apellido</th>
<th>Correo</th>
<th>Rol</th>
<th>País</th>
<th></th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop