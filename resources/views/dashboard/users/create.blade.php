@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h5>Agregar nuevo usuario para gestión del sistema</h5>

</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('users.store')}}">
{{ csrf_field() }}
<input type="hidden" name="status" value="2">
<input type="hidden" name="terms" value="on">
<h3> Registration </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="email-2" class="block">Email *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="email-2" name="email" type="email" class="required form-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="password-2" class="block">Contraseña*</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="password-2" name="password" type="password" class="required form-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="confirm-2" class="block">Confirmar Contraseña *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="confirm-2" name="confirm" type="password" class="required form-control">
</div>
</div>
</fieldset>

<h3> Información General </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="name-2" class="block">Nombres *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="name-2" name="name" type="text" class="form-control required">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="surname-2" class="block">Apellidos *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="surname-2" name="lastName" type="text" class="form-control required">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="phone-2" class="block">Telefono #</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="phone-2" name="phone" type="number" class="form-control required phone">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="phone-2" class="block"> Tipo de usuario</label>
</div>
<div class="col-md-8 col-lg-10">
<select name="role" class="form-control" required>
	@if(Auth::user()->role == 1000)
	<option value="1000"> Administrador</option>
 	<option value="500"> Moderador</option>
  	<option value="501" disabled=""> Editor Web</option>
  	<option value="502"> Atención al cliente</option>
  	@endif
  	<option value="2"> Cliente</option>
</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="phone-2" class="block">Pais de nacimiento </label>
</div>
<div class="col-md-8 col-lg-10">
<select class="form-control" id="selectPais" name="country" required>
	@foreach($paises as $pais)
	<option value="{{$pais->PaisCodigo}}">{{$pais->PaisNombre}}</option>
	
	@endforeach
</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Date Of Birth</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="birthDay" type="date" class="form-control required date-control" max="<?php 
	$fecha = date('Y-m-j');
	$nuevafecha = strtotime ( '-18 year' , strtotime ( $fecha ) ) ;
	$nuevafecha = date ( 'Y-m-j' , $nuevafecha );
	echo $nuevafecha;

 ?>">
</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>


@stop