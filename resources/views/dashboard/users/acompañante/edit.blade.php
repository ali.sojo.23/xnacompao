@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h5>Editar Usuario</h5>
<span>Completa los Campos requeridos</span>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('users.update',['user'=>encrypt(Auth::user()->id)])}}" enctype="multipart/form-data">
{{ method_field('PUT')}}
{{ csrf_field() }}
<input type="hidden" name="status" value="2">
<h3> Información General </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="name-2" class="block">Primer Nombre *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="name-2" name="name" type="text" class="form-control required" value="{{$users->name}}" <?php if(!empty($users->lastName)) : ?> disabled <?php endif ?>>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="surname-2" class="block">Apellidos *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="surname-2" name="lastName" type="text" class="form-control required" <?php if(!empty($users->lastName)) : ?> value="{{$users->lastName}}" disabled <?php endif ?>>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-3">
<label for="phone-2" class="block">Cedula de identidad</label>
</div>
<div class="col-md-8 col-lg-9">
<input id="phone-2" name="cedula" type="number" class="form-control required phone">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Fecha de Nacimiento</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="birthDay" type="date" class="form-control required date-control" placeholder="{{$users->date}}">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="email-2" class="block"> Sexo *</label>
</div>
<div class="col-md-8 col-lg-10">
<select class="form-control" id="selectDomicilioHoras" name="sexo" required>
 	<option value="1"> Masculino</option>
  <option value="2"> Femenino</option>
</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Foto de Perfil</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="file" name="avatar" type="file" class="form-control <?php if(!empty($user->avatar)) : ?> required <?php endif ?> date-control" value="{{$users->avatar}}">
</div>
</div>
</fieldset>
<h3> Cobertura Laboral Y Ubicación </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-4">
<label for="email-2" class="block"> Area de trabajo</label>
</div>
<div class="col-md-8 col-lg-8">
<select class="form-control" id="selectDepartamento" name="servicios[]" required multiple="">
	@foreach($users->pais->tasaCambio->servicios as $servicio)
			<option value="{{$servicio->id}}">{{$servicio->servicio}}</option>
	@endforeach
</select>
</div>
</div>

<div class="form-group row">
<div class="col-md-4 col-lg-4">
<label for="email-2" class="block"> Estado/Departamento/Ciudad</label>
</div>
<div class="col-md-8 col-lg-8">
<select class="form-control" id="selectDepartamento" name="estado" required>
	@foreach($users->pais->ciudades as $estado)
			<option value="{{$estado->CiudadID}}">{{$estado->CiudadNombre}}</option>
	@endforeach
</select>
</div>
</div>
</fieldset>

</form>
</section>
</div>
</div>
</div>
</div>
</div>


@stop