@extends('Templates.Dashboard.users')
@section('body')
<?php 
$pais1=$pais; 
$pais2=$pais; 
?>
<div class="card">
<div class="card-header">
<h5>Experiencia Laboral</h5>
<span>Complete los Campos requeridos</span>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('workExperiencePublic',['id'=>encrypt(Auth::user()->id)])}}" enctype="multipart/form-data">
{{ method_field('PUT')}}
{{ csrf_field() }}
<h3> Experiencia Laboral </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="Company-2" class="block">Empresa:</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="Company-2" name="work1" type="text" class="form-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="CountryW-2" class="block">País</label>
</div>
<div class="col-md-8 col-lg-10">
<select class="form-control" id="selectPais" name="countryWork1">
	<option value="">-- Seleccione un País --</option>
  @foreach($pais as $pais)
    <option value="{{$pais->PaisCodigo}}">{{$pais->PaisNombre}}</option>
  @endforeach
</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="Position-2" class="block">Cargo</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="Position-2" name="cargoWork1" type="text" class="form-control">

</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Fecha de Inicio</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="inicioWork1" type="date" class="form-control date-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Fecha de Finalización</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="finWork1" type="date" class="form-control date-control">
</div>
</div>
</fieldset>
<h3> Experiencia Laboral 2</h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="Company-2" class="block">Empresa:</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="Company-2" name="work2" type="text" class="form-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="CountryW-2" class="block">País</label>
</div>
<div class="col-md-8 col-lg-10">
<select class="form-control" id="selectPais2" name="countryWork1">
	<option value="">-- Seleccione un País --</option>
@foreach($pais2 as $pais2) 
    <option value="{{$pais2->PaisCodigo}}">{{$pais2->PaisNombre}}</option>
@endforeach
</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="Position-2" class="block">Cargo</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="Position-2" name="cargoWork2" type="text" class="form-control">

</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Fecha de Inicio</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="inicioWork2" type="date" class="form-control date-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Fecha de Finalización</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="finWork2" type="date" class="form-control date-control">
</div>
</div>
</fieldset>
<h3> Experiencia Laboral </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="Company-2" class="block">Empresa:</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="Company-2" name="work3" type="text" class="form-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="CountryW-2" class="block">País</label>
</div>
<div class="col-md-8 col-lg-10">
<select class="form-control" id="selectPais2" name="countryWork1">
	<option value="">-- Seleccione un País --</option>
@foreach($pais1 as $pais1) 
    <option value="{{$pais1->PaisCodigo}}">{{$pais1->PaisNombre}}</option>
@endforeach
</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="Position-2" class="block">Cargo</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="Position-2" name="cargoWork3" type="text" class="form-control">

</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Fecha de Inicio</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="inicioWork3" type="date" class="form-control date-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Fecha de Finalización</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="finWork3" type="date" class="form-control date-control">
</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>


@stop