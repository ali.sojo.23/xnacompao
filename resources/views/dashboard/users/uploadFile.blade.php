@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h5>Documentos de Validación</h5>
<span>Estos documentos serán revisados por un moderador especializado de <a href="https://acompaño.com">acompaño.com</a> adicionalmente serán visualizados por la persona contratante.</span>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('docsStore',['id'=>encrypt(Auth::user()->id)])}}" enctype="multipart/form-data">
{{ method_field('PUT')}}
{{ csrf_field() }}
<h3> Documentos de Validanción </h3>
<fieldset>

<div class="form-group row">
<div class="col-md-4 col-lg-12">
<label for="date" class="block">Documento de Identficación <br><small>Ingrese archivo en formato <code>.jpeg</code> Tamaño máximo del archivo 4MB</small></label>
</div>
<div class="col-md-8 col-lg-12">
<input id="file" name="cedula" type="file" class="form-control required date-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-12">
<label for="date" class="block">Curriculum Laboral <br><small>Ingrese archivo en formato <code>.doc</code> o <code>.pdf</code> Tamaño máximo del archivo 4MB</small></label>
</div>
<div class="col-md-8 col-lg-12">
<input id="file" name="curriculo" type="file" class="form-control required date-control">
</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>


@stop