@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Usuarios Registrados</h5>
<span>En la siguiente lista puede observar las personas registradas en sitio web, puede usar el buscador para filtrar por: Nombre, Apellido, Rol y País de residencia.</span>
</div>
<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>#</th>
<th>Nombre</th>
<th>apellido</th>
<th>Correo</th>
<th>Rol</th>
<th>País</th>
<th></th>
</tr>
</thead>
<tbody>
<?php $contador = 0 ?>
@foreach($users as $user)
<tr>
<td>{{$contador++}}</td>
<td>{{$user->name}}</td>
<td>{{$user->lastName}}</td>
<td>{{$user->email}}</td>
<td>
@if($user->role == 1)
	Cuidador
@endif
@if($user->role == 2)
	Cliente
@endif
@if($user->role == 500)
	Moderador
@endif
@if($user->role == 502)
	Atención al cliente
@endif
@if($user->role == 1000)
	Administrador
@endif
</td>
<td>{{$user->pais->PaisNombre}}</td>
<td><a href="{{route('users.edit',['id'=>encrypt($user->id)])}}" class="text-info">Editar</a> | <a class="text-danger" href="{{ route('users.destroy',['user'=>encrypt($user->id)]) }}"
 	onclick="event.preventDefault();
 		document.getElementById('delete{{$user->id}}').submit();">
Eliminar
</a>
<form id="delete{{$user->id}}" action="{{ route('users.destroy',['user'=>encrypt($user->id)]) }}" method="POST" style="display: none;">
	{{ method_field('DELETE')}}
  	{{ csrf_field() }}
</form>
</td>
</tr>

@endforeach
</tbody>
<tfoot>
<tr>
<th>Nombre</th>
<th>apellido</th>
<th>Correo</th>
<th>Rol</th>
<th>País</th>
<th></th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop