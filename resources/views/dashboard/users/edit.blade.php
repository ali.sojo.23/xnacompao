@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h5>Form wizard with Validation</h5>
<span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('users.update',['user'=>$users->id])}}" enctype="multipart/form-data">
{{ method_field('PUT')}}
{{ csrf_field() }}
@if($users->id==1)
<input type="hidden" name="status" value="2">
@endif
<h3> Registration </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="email-2" class="block">Email *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="email-2" name="email" type="email" class="required form-control" <?php if($users) : ?> placeholder="{{$users->email}}" disabled <?php endif ?>>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="password-2" class="block">Password *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="password-2" name="password" type="password" class="form-control">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="confirm-2" class="block">Confirm Password *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="confirm-2" name="confirm" type="password" class="form-control">
</div>
</div>
</fieldset>

<h3> General information </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="name-2" class="block">First name *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="name-2" name="name" type="text" class="form-control required" value="{{$users->name}}" <?php if(!empty($users->lastName)) : ?> disabled <?php endif ?>>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="surname-2" class="block">Last name *</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="surname-2" name="lastName" type="text" class="form-control required" <?php if(!empty($users->lastName)) : ?> value="{{$users->lastName}}" disabled <?php endif ?>>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="phone-2" class="block">Phone #</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="phone-2" name="phone" type="number" class="form-control required phone" value="{{$users->phone}}">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Date Of Birth</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="date" name="birthDay" type="date" class="form-control required date-control" placeholder="{{$users->date}}">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Date Of Birth</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="file" name="avatar" type="file" class="form-control <?php if(!empty($user->avatar)) : ?> required <?php endif ?> date-control" value="{{$users->avatar}}">
</div>
</div>
</fieldset>
<h3> Work experience </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="Company-2" class="block">Company:</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="Company-2" name="company" type="text" class="form-control required" value="{{$users->company}}">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="CountryW-2" class="block">Country</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="CountryW-2" name="country" type="text" class="form-control required" value="{{$users->country}}">
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="Position-2" class="block">Position</label>
</div>
<div class="col-md-8 col-lg-10">
<input id="Position-2" name="position" type="text" class="form-control required" value="{{$users->position}}">
</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>


@stop