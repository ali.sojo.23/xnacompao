@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Usuarios registrados pendientes por completar formulario de contacto</h5>
<span>En la siguiente lista puede observar las personas registradas en sitio web, puede usar el buscador para filtrar por: Nombre, Apellido, Rol y País de residencia.</span>
</div>
<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>#</th>
<th>Nombre</th>
<th>apellido</th>
<th>Correo</th>
<th>Rol</th>
<th>País</th>
</tr>
</thead>
<tbody>
<?php $id = 1; ?>
@foreach($users as $user)
@if($user->status == 1)
@if($user->role == 1 || $user->role == 2)
<tr>
<td>{{ $id++ }}</td>
<td>{{$user->name}}</td>
<td>{{$user->lastName}}</td>
<td>{{$user->email}}</td>
<td>
@if($user->role == 1)
	Cuidador
@endif
@if($user->role == 2)
	Cliente
@endif
</td>
<td>{{$user->country}}</td>
</tr>
@endif
@endif
@endforeach
</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Nombre</th>
<th>apellido</th>
<th>Correo</th>
<th>Rol</th>
<th>País</th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop