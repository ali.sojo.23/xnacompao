@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Tabla de Comisiones por paises</h5>
<span>En la siguiente lista puede observar la tabla de comisiones según el porcentual de beneficio que recibirá al realizar una venta de la membresía</span>
</div>
<?php $id = 0; ?>
@foreach($paises as $pais)
<div class="card-block">
<div class="dt-responsive table-responsive">
<h3><?php echo $pais->Pais->PaisNombre ?></h3>
<table class="simpletable table table-striped table-bordered nowrap">
<thead>
<tr>
<th>Membresia Nombre</th>
<th>Tipo</th>
<th>Costo de membresía</th>
<th> 1 - 10<small> ventas</small> <br><small>Todas las ventas</small></th>
<th> 11 - 15<small> ventas</small> <br><small>Apartir de la venta 11</small></th>
<th> 16 - 20<small> ventas</small> <br><small>Todas las Ventas</small></th>
<th> 21 - 25<small> ventas</small> <br><small>Apartir de la venta 21</small></th>
<th> 26 - 40<small> ventas</small> <br><small>Todas las ventas</small></th>
<th>  41 + <small> ventas</small> <br><small>Todas las ventas</small></th>
</tr>
</thead>
<tbody>

@foreach($comisiones[$pais->id] as $comision)
<tr>
<td><?php echo $comision->membresia->membresia . " " . $comision->hora . " " ?></td>
<td><?php if($comision->membresia->profesional == 0): echo "No profesional"; else: echo "Profesional"; endif;?></td>
<td><?php echo $comision->suscripcion . " USD" ?></td>
<td><?php echo round($comision->suscripcion*.2) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.25) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.25) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.3) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.3) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.5) . " USD" ?> </td>
</tr>
@endforeach
</tbody>
<tfoot>
<tr>
<th>Membresia Nombre</th>
<th>tipo</th>
<th> 1 - 10<small> ventas</small> <br><small>Todas las ventas</small></th>
<th> 11 - 15<small> ventas</small> <br><small>Apartir de la venta 11</small></th>
<th> 16 - 20<small> ventas</small> <br><small>Todas las Ventas</small></th>
<th> 21 - 25<small> ventas</small> <br><small>Apartir de la venta 21</small></th>
<th> 26 - 40<small> ventas</small> <br><small>Todas las ventas</small></th>
<th>  41 + <small> ventas</small> <br><small>Todas las ventas</small></th>
</tr>
</tfoot>
</table>
</div>
<small>Si cumple con la meta de +40 ventas mensuales, recibirá 5USD por cada venta que reciba el mes siguiente</small>
</div>
<?php $id++ ?>
@endforeach
</div>
@stop