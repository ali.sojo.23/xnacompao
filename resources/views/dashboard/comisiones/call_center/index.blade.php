@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Tabla de Comisiones por paises</h5>
<span>En la siguiente lista puede observar la tabla de comisiones según el porcentual de beneficio que recibirá al realizar una venta de la membresía</span>
</div>
<?php $id = 0; ?>
@foreach($paises as $pais)
<div class="card-block">
<div class="dt-responsive table-responsive">
<h3><?php echo $pais->Pais->PaisNombre ?></h3>
<table class="simpletable table table-striped table-bordered nowrap">
<thead>
<tr>
<th>Membresia Nombre</th>
<th>Costo de <br>membresía</th>
<th> 1 - 5<small> ventas</small> </th>
<th> 6 - 10<small> ventas</small> </th>
<th> 11 - 15<small> ventas</small> </th>
<th> 15 - 19<small> ventas</small> </th>
<th> 20 +<small> ventas</small></th>
</tr>
</thead>
<tbody>

@foreach($comisiones[$pais->id] as $comision)
<tr>
<td><?php echo $comision->membresia->membresia . " " . $comision->hora . " " ?> <br>
	<?php if($comision->membresia->profesional == 0): echo "No profesional"; else: echo "Profesional"; endif;?>
</td>
<td><?php echo $comision->suscripcion . " USD" ?></td>
<td><?php echo round($comision->suscripcion*.5) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.6) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.7) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.8) . " USD" ?> </td>
<td><?php echo round($comision->suscripcion*.9) . " USD" ?> </td>
</tr>
@endforeach
</tbody>
<tfoot>
<tr>
<th>Membresia Nombre</th>
<th>Costo de <br>membresía</th>
<th> 1 - 5<small> ventas</small> </th>
<th> 6 - 10<small> ventas</small> </th>
<th> 11 - 15<small> ventas</small> </th>
<th> 15 - 19<small> ventas</small> </th>
<th> 20 +<small> ventas</small></th>
</tr>
</tfoot>
</table>
</div>
<small>Si cumple con la meta de +40 ventas mensuales, recibirá 5USD por cada venta que reciba el mes siguiente</small>
</div>
<?php $id++ ?>
@endforeach
</div>
@stop