@extends('Templates.Dashboard.index')
@section('body')
<!-- Información -->
<div class="row">

<div class="col-lg-12">

<div class="job-card card-columns">
@foreach($membresias as $membresia)
<?php if($membresia->costo->cambio->Pais->PaisCodigo == Auth::user()->country): ?> 
<div class="col-sm-12">
<div class="card card-border-primary">
<div class="card-header">
<h3><?php echo $membresia->costo->membresia->membresia . " " . $membresia->costo->hora ?></h3>
<!-- <span class="label label-default f-right"> 28 January, 2015 </span> -->
</div>
<div class="card-block">
<div class="row">
<div class="col-sm-6">
<ul class="list list-unstyled">

<li>Horas: <?php echo $membresia->costo->hora ?> Horas</li>
<li>Dias: <span class="text-semibold"><?php echo $membresia->costo->dias ?></span> dias</li>
</ul>
</div>
<div class="col-sm-6">
<ul class="list list-unstyled text-right">
<li>Servicio: <span class="text-semibold">
</span></li>
</ul>
</div>
</div>
<div class="row">
	<div class="col-sm-6">
		Costo Mensualidad: <h5><?php echo round($membresia->costo->mensualidad*$membresia->costo->cambio->tasaCambios) . " " . $membresia->costo->cambio->monedaLocal ?></h5>
	</div>
	<div class="col-sm-6">
		Costo Suscripción: <h5><?php echo round($membresia->costo->suscripcion*$membresia->costo->cambio->tasaCambios) . " " . $membresia->costo->cambio->monedaLocal ?></h5>
	</div>
</div>
</div>
<div class="card-footer">
<div class="task-board m-0">
<a href="{{ route('invoice.show',['invoice'=>encrypt($membresia->id),'tipo'=>'membresia'])}}" class="btn btn-info btn-mini b-none"><i class="icofont icofont-eye-alt m-0"></i></a>
<!-- end of dropdown-secondary -->
</div>
</div>
</div>
</div>

<?php endif ?>
@endforeach
</div>
</div>
</div>


 @stop