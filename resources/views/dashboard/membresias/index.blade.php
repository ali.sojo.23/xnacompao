@extends('Templates.Dashboard.index')
@section('body')
<!-- Información -->
<div class="row">

<div class="col-lg-12">

<div class="job-card card-columns">
@foreach($membresia as $membresia)
<?php if($membresia->cambio->Pais->PaisCodigo == Auth::user()->country): ?> 
<div class="col-sm-12">
<div class="card card-border-primary">
<div class="card-header">
<h3><?php echo $membresia->membresia->membresia . " " . $membresia->hora ?></h3>
<!-- <span class="label label-default f-right"> 28 January, 2015 </span> -->
</div>
<div class="card-block">
<div class="row">
<div class="col-sm-6">
<ul class="list list-unstyled">

<li>Horas: <?php echo $membresia->hora ?> Horas</li>
<li>Dias: <span class="text-semibold"><?php echo $membresia->dias ?></span> dias</li>
</ul>
</div>
<div class="col-sm-6">
<ul class="list list-unstyled">
<li>Servicio: <span class="text-semibold"><?php 

if($membresia->membresia->profesional == 1):
	echo "Profesional";
else :
	echo "No Profesional";
endif;

 ?></span></li>
</ul>
</div>
</div>
<div class="row">
	<div class="col-sm-6">
		Costo Mensualidad: <h5><?php echo round($membresia->mensualidad*$membresia->cambio->tasaCambios) . " " . $membresia->cambio->monedaLocal ?></h5>
	</div>
	<div class="col-sm-6">
		Costo Suscripción: <h5><?php echo round($membresia->suscripcion*$membresia->cambio->tasaCambios) . " " . $membresia->cambio->monedaLocal ?></h5>
	</div>
</div>
</div>
<div class="card-footer">
<div class="task-board m-0">
<a href="{{ route('membresias.store')}}" class="btn btn-info b-none" 
 	onclick="event.preventDefault();
 		document.getElementById('Moderar<?php echo $membresia->id ?>').submit();">
Contratar</a>
<form id="Moderar<?php echo $membresia->id ?>" action="{{ route('membresias.store')}}" method="POST" enctype="multipart/form-data" style="display: none;">
  	{{ csrf_field() }}
  	<input type="hidden" value="{{Auth::user()->id}}" name="usuario">
  	<input type="hidden" value="{{$membresia->id}}" name="membresiaCosto">
  	<input type="hidden" value="{{$membresia->dias}}" name="dias_disponible">
</form>
<!-- end of dropdown-secondary -->
</div>
</div>
</div>
</div>

<?php endif ?>
@endforeach
</div>
</div>
</div>


 @stop