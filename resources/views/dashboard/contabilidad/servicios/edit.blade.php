@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h3>Crear Membresia</h3>
<p>Antes de crear una membresía y burcar por Pais, usted deberá crear la Tasa de Cambio. Reecuerde la tasa de cambio debe ser creada con un valor por defecto de 1USD$</p>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('servicios.update',['servicio'=>encrypt($servicio->id)])}}" enctype="multipart/form-data">
{{ method_field('PUT')}}
{{ csrf_field() }}
<h3>Costos de Servicio</h3>

<fieldset>

<div class="form-group">
<div class="row">	
<div class="col-md-4 col-lg-3">
<label for="CountryW-2" class="block">Pais</label>
</div>
<div class="col-md-8 col-lg-9">
<input type="text" class="form-control" name="id_pais" disabled="" value="<?php echo $servicio->pais->Pais->PaisNombre?>">
</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-3">
<label for="date" class="block">Servicio</label>
</div>
<div class="col-md-8 col-lg-9">
<div class="input-group">
<input id="" name="servicio" type="text" class="form-control date-control required" value="<?php echo $servicio->servicio ?>">
</div>
</div>
</div>
</div><div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Costo No Profesional</label>
</div>
<div class="col-md-8 col-lg-4">
<div class="input-group">
<input id="" name="costo" type="text" class="form-control date-control required" value="<?php echo $servicio->costo ?>">
</div>
</div>
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Costo Profesional</label>
</div>
<div class="col-md-8 col-lg-4">
<div class="input-group">
<input id="" name="costo_profesional" type="text" class="form-control date-control required" value="<?php echo $servicio->costo_profesional ?>">
</div>
</div>

</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>

@stop