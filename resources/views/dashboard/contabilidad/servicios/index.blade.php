@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Membresias</h5>
</div>

<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>#</th>
<th>Servicio</th>
<th>Pais</th>
<th>Costo</th>
<th>Costo Profesional</th>
<th>Acciones</th>
</tr>
</thead>
<tbody>
<?php $id = 1 ?>
@foreach($servicios as $servicio)
<tr>

	<td><?php echo $id++; ?></td>
	<td>{{$servicio->servicio}}</td>
	<td>{{$servicio->pais->Pais->PaisNombre}}</td>
	<td><?php echo $servicio->costo*$servicio->pais->tasaCambios . " " . $servicio->pais->monedaLocal ?></td>
	<td><?php echo $servicio->costo_profesional*$servicio->pais->tasaCambios . " " . $servicio->pais->monedaLocal ?></td>
	<td>
	<a class="text-info" href="{{route('servicios.edit',['servicio'=>encrypt($servicio->id)])}}">Editar Servicio</a> <br> 

	<a class="text-danger" href="{{ route('servicios.destroy',['servicio'=>encrypt($servicio->id)]) }}"
 	onclick="event.preventDefault();
 		document.getElementById('delete<?php echo $servicio->id  ?>').submit();">
		Eliminar Servicio
	</a>
	<form id="delete<?php echo $servicio->id  ?>" action="{{ route('servicios.destroy',['servicio'=>encrypt($servicio->id)]) }}" method="POST" enctype="multipart/form-data" style="display: none;">
		{{ method_field('DELETE')}}
  		{{ csrf_field()}}
	</form>

	</td>
</tr>
@endforeach
</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Servicio</th>
<th>Pais</th>
<th>Costo</th>
<th>Costo Profesional</th>
<th>Acciones</th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop