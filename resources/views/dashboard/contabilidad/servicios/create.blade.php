@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h3>Crear Servicios</h3>
<p>Antes de crear una servicio y burcar por Pais, usted deberá crear la Tasa de Cambio. Reecuerde la tasa de cambio debe ser creada con un valor por defecto de 1USD$</p>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('servicios.store')}}" enctype="multipart/form-data">
{{ csrf_field() }}
<h3>Costos de Servicio</h3>

<fieldset>

<div class="form-group">
<div class="row">	
<div class="col-md-4 col-lg-3">
<label for="CountryW-2" class="block">Pais</label>
</div>
<div class="col-md-8 col-lg-9">
<select class="form-control required" id="selectPais" name="id_pais">
	<option value="">-- Seleccione un Pais --</option>
	@foreach($paises as $pais)
    <option value="{{$pais->id}}">{{$pais->Pais->PaisNombre}}</option>
    @endforeach
</select>
</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-3">
<label for="date" class="block">Servicio</label>
</div>
<div class="col-md-8 col-lg-9">
<div class="input-group">
<input id="" name="servicio" type="text" class="form-control date-control required">
</div>
</div>
</div>
</div><div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Costo No Profesional</label>
</div>
<div class="col-md-8 col-lg-4">
<div class="input-group">
<input id="" name="costo" type="text" class="form-control date-control required">
</div>
</div>
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Costo Profesional</label>
</div>
<div class="col-md-8 col-lg-4">
<div class="input-group">
<input id="" name="costo_profesional" type="text" class="form-control date-control required">
</div>
</div>

</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>

@stop