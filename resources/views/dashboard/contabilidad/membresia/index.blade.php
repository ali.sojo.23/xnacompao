@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Membresias</h5>
</div>

<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>#</th>
<th>Pais</th>
<th>Nivel de Estudio</th>
<th>Membresia</th>
<th>Acciones</th>
</tr>
</thead>
<tbody>
<?php $id = 1 ?>
@foreach($membresias as $membresia)
<tr>

	<td><?php echo $id++; ?></td>
	<td>{{$membresia->pais->PaisNombre}}</td>
	<td><?php 
		if($membresia->profesional == 1):
			echo "Profesional";
		else:
			echo "No Profesional";
		endif;
	 ?></td>
	<td>{{$membresia->membresia}}</td>
	<td>
	<a class="text-info" href="{{route('costoMembresia.create',['id'=>encrypt($membresia->id)])}}">Crear Precios</a> <br>
	<a class="text-success" href="{{route('costoMembresia.show',['id'=>encrypt($membresia->id)])}}">Ver Precios</a> <br>
	<a class="text-info" href="{{route('membresia.edit',['membresium'=>encrypt($membresia->id)])}}">Editar Membresia</a> <br> 

	<a class="text-danger" href="{{ route('membresia.destroy',['membresium'=>encrypt($membresia->id)]) }}"
 	onclick="event.preventDefault();
 		document.getElementById('delete<?php echo $membresia->id  ?>').submit();">
		Eliminar Membresia
	</a>
	<form id="delete<?php echo $membresia->id  ?>" action="{{ route('membresia.destroy',['membresium'=>encrypt($membresia->id)]) }}" method="POST" enctype="multipart/form-data" style="display: none;">
		{{ method_field('DELETE')}}
  		{{ csrf_field()}}
	</form>

	</td>
</tr>
@endforeach
</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Pais</th>
<th>Membresia</th>
<th>Acciones</th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop