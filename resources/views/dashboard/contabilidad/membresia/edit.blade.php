@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h3>Crear Membresia</h3>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('membresia.update',['membresium'=>encrypt($membresia->id)])}}" enctype="multipart/form-data">
{{ method_field('PUT')}}
{{ csrf_field() }}
<h3>Costos de Servicio</h3>
<fieldset>

<div class="form-group">
<div class="row">	
<div class="col-md-4 col-lg-3">
<label for="CountryW-2" class="block">Pais</label>
</div>
<div class="col-md-8 col-lg-9">
<select class="form-control required" id="selectPais" name="idPais" disabled="">
	<option value="{{$membresia->pais->PaisCodigo}}"> {{$membresia->pais->PaisNombre}} </option>
</select>
</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-3">
<label for="date" class="block">Nombre de Membresía</label>
</div>
<div class="col-md-8 col-lg-9">
<div class="input-group">
<input value="{{$membresia->membresia}}" name="membresia" type="text" class="form-control date-control required">
</div>
</div>
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Membresía Profesional</label>
</div>
<div class="col-md-8 col-lg-2">
<div class="input-group">
<input <?php if($membresia->profesional): echo "checked"; endif; ?> id="" name="profesional" value="1" type="checkbox" class="form-check-input ">
</div>
</div>

</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>

@stop