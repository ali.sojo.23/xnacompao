@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h3>Crear Membresia</h3>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('costoMembresia.store')}}" enctype="multipart/form-data">
<input type="hidden" name="membresia" value="{{$membresia->id}}">
{{ csrf_field() }}
<h3>Costos de Servicio</h3>
<fieldset>
<div class="form-group">
<div class="row">	
<div class="col-md-4 col-lg-3">
<label for="CountryW-2" class="block">Membresia Contratada</label>
</div>
<div class="col-md-8 col-lg-9">
<input value="{{$membresia->membresia}}" id="" type="text" class="form-control date-control required" disabled="">
</div>
</div>
<div class="row">	
<div class="col-md-4 col-lg-3">
<label for="CountryW-2" class="block">Pais de la Membresia</label>
</div>
<div class="col-md-8 col-lg-9">
<select class="form-control required" id="selectPais" name="tasacambio_id[]" multiple="">
	<option value="">-- Seleccione Cantidad de Horas --</option>
    @foreach($tasas as $tasa)
    <option value="{{$tasa->id}}">{{$tasa->pais->PaisNombre}}</option>
    @endforeach
</select>
</div>
</div>
</div>
<div class="form-group">
<div class="row">	
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Horas por Membresía</label>
</div>
<div class="col-md-8 col-lg-5">
<select class="form-control required" id="selectPais" name="horas">
	<option value="">-- Seleccione Cantidad de Horas --</option>
    <option value="4">4 horas</option>
    <option value="8">8 horas</option>
    <option value="12">12 horas</option>
    <option value="16">16 horas</option>
    <option value="24">24 horas</option>
</select>
</div>
<div class="col-md-4 col-lg-1">
<label for="CountryW-2" class="block">Dias por Membresía</label>
</div>
<div class="col-md-8 col-lg-5">
<input type="number" class="form-control" name="dias">
</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-md-1 col-lg-1">
<label for="date" class="block">Precio de suscripcion</label>
</div>
<div class="col-md-5 col-lg-5">
<div class="input-group">
<input id="" name="suscripcion" type="text" class="form-control date-control required">
</div>
</div>
<div class="col-md-1 col-lg-1">
<label for="date" class="block">Precio de Mensualidad</label>
</div>
<div class="col-md-5 col-lg-5">
<div class="input-group">
<input id="" name="mensualidad" type="text" class="form-control date-control required">
</div>
</div>

</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>

@stop