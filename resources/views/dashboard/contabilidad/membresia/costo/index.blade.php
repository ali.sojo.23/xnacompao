@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Costo de Membresias</h5>
</div>

<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>#</th>
<th>Membresia</th>
<th>Profesional</th>
<th>Dias</th>
<th>Horas</th>
<th>Pais</th>
<th>Mensualidad</th>
<th>Suscripcion</th>
<th>Acciones</th>
</tr>
</thead>
<tbody>
<?php $id = 1 ?>
@foreach($costos as $costo)
<tr>

	<td> <?php echo $costo->id ?></td>
	<td>{{$costo->membresia->membresia}}</td>
	<td>{{$costo->membresia->profesional}} </td>
	<td>{{$costo->dias}} Dias</td>
	<td>{{$costo->hora}} Horas</td>
	<td>{{$costo->cambio->Pais->PaisNombre}}</td>
	<td><?php echo $costo->mensualidad*$costo->cambio->tasaCambios ?> {{$costo->cambio->monedaLocal}}</td>
	<td><?php echo $costo->suscripcion*$costo->cambio->tasaCambios ?> {{$costo->cambio->monedaLocal}}</td>
	<td>
	<a class="text-info" href="{{route('costoMembresia.edit',['membresium'=>encrypt($costo->id)])}}">Editar</a>
    <br> 
	<a class="text-danger" href="{{ route('costoMembresia.destroy',['membresium'=>encrypt($costo->id)]) }}"
 	onclick="event.preventDefault();
 		document.getElementById('delete<?php echo $costo->id ?>').submit();">
		Eliminar
	</a>
	<form id="delete<?php echo $costo->id ?>" action="{{ route('costoMembresia.destroy',['membresium'=>encrypt($costo->id)]) }}" method="POST" enctype="multipart/form-data" style="display: none;">
		{{ method_field('DELETE')}}
  		{{ csrf_field()}}
	</form>

	</td>
</tr>
@endforeach
</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Membresia</th>
<th>Profesional</th>
<th>Dias</th>
<th>Horas</th>
<th>Pais</th>
<th>Mensualidad</th>
<th>Suscripcion</th>
<th>Acciones</th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop