@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h3>Editar Membresia</h3>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('costoMembresia.update',['costoMembresium'=>encrypt($costo->id)])}}" enctype="multipart/form-data">
{{ method_field('PUT')}}
{{ csrf_field() }}
<h3>Costos de Servicio</h3>
<fieldset>
<div class="form-group">
<div class="row">	
<div class="col-md-4 col-lg-3">
<label for="CountryW-2" class="block">Membresia Contratada</label>
</div>
<div class="col-md-8 col-lg-9">
<input value="{{$costo->membresia->membresia}}" id="" type="text" class="form-control date-control required" disabled="">
</div>
</div>
</div>
<div class="form-group">
<div class="row">	
<div class="col-md-4 col-lg-3">
<label for="CountryW-2" class="block">Horas por Membresía</label>
</div>
<div class="col-md-8 col-lg-9">
<select class="form-control required" id="selectPais" name="horas" disabled="">
	<option value="">{{$costo->hora}} Horas</option>

</select>
</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-md-1 col-lg-1">
<label for="date" class="block">Precio de suscripcion</label>
</div>
<div class="col-md-5 col-lg-5">
<div class="input-group">
<input id="" name="suscripcion" type="text" class="form-control date-control required" value="{{$costo->suscripcion}}">
</div>
</div>
<div class="col-md-1 col-lg-1">
<label for="date" class="block">Precio de Mensualidad</label>
</div>
<div class="col-md-5 col-lg-5">
<div class="input-group">
<input id="" name="mensualidad" type="text" class="form-control date-control required" value="{{$costo->mensualidad}}">
</div>
</div>

</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>

@stop