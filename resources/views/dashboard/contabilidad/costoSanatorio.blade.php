@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Costo de servicios en Sanatorios según pais</h5>
</div>
<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>#</th>
<th>Pais</th>
<th>Costo del Servicio en Dolares</th>
<th>Tasa de Cambio según Pais</th>
<th>Costo de Servicio en Modena Local</th>
<th></th>
</tr>
</thead>
<?php 
$id = 0;
 ?>
<tbody>
@foreach($costo as $costo)

<tr>
<td>{{$id= $id+1}}</td>
<td>{{$costo->idPais}} </td>
<td>{{$costo->costoDolar}} USD</td>
<td>{{$costo->tasaCambio}} {{$costo->monedaLocal}}</td>
<td>{{round($costo->tasaCambio * $costo->costoDolar)}} {{$costo->monedaLocal}}</td>
<td></td>
</tr>

@endforeach
</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Pais</th>
<th>Costo del Servicio en Dolares</th>
<th>Tasa de Cambio según Pais</th>
<th>Costo de Servicio en Modena Local</th>
<th></th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop