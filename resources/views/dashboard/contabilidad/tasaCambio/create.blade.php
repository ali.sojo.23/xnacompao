@extends('Templates.Dashboard.users')
@section('body')
<?php 
$pais1=$pais; 
$pais2=$pais; 
?>
<div class="card">
<div class="card-header">
<h5>Crear Costo de Servicios</h5>
<span>Crear costos de Servicio según país y tipo</span>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="POST" action="{{route('tasadecambio.store')}}" enctype="multipart/form-data">
{{ csrf_field() }}
<h3>Costos de Servicio</h3>
<fieldset>

<div class="form-group">
<div class="row">	
<div class="col-md-4 col-lg-2">
<label for="CountryW-2" class="block">Pais</label>
</div>
<div class="col-md-8 col-lg-4">
<select class="form-control required" id="selectPais" name="idPais">
	<option value="">-- Seleccione un Pais --</option>
	@foreach($pais as $pais)
    <option value="{{$pais->PaisCodigo}}">{{$pais->PaisNombre}}</option>
    @endforeach
</select>
</div>
<div class="col-md-4 col-lg-2">
<label for="date" class="block">TAX</label>
</div>
<div class="col-md-8 col-lg-4">
<div class="input-group">
<input id="" name="tax" type="text" class="form-control date-control required">
<span class="input-group-addon" id="basic-addon3">%</span>
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Tasa de Cambio</label>
</div>
<div class="col-md-8 col-lg-4">
<div class="input-group">
<input id="" name="tasaCambio" type="text" class="form-control date-control required">
<span class="input-group-addon" id="basic-addon3">$</span>
</div>
</div>
<div class="col-md-4 col-lg-2">
<label for="date" class="block">Moneda local</label>
</div>
<div class="col-md-8 col-lg-4">

<input placeholder="Ex: VED, ARS" id="" name="monedaLocal" type="text" class="form-control date-control required">


</div>
</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>

@stop