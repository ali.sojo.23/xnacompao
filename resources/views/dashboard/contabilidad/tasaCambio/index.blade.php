@extends('Templates.Dashboard.table')
@section('body')
<div class="card">
<div class="card-header">
<h5>Tasa de cambio tomando como moneda referencial Dollar</h5>
</div>

<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>#</th>
<th>Pais</th>
<th>TAX</th>
<th>Tasa de Cambio según Pais</th>
<th>Modena Local</th>
<th>Acciones</th>
</tr>
</thead>
<tbody>
@foreach($tasas as $tasa)
<tr>

	<td>{{$tasa->id}}</td>
	<td>{{$tasa->pais->PaisNombre}}</td>
	<td>{{$tasa->tax*100}}%</td>
	<td>{{$tasa->tasaCambios}}</td>
	<td>{{$tasa->monedaLocal}}</td>
	<td>

	<a class="text-info" href="{{route('tasadecambio.edit',['tasadecambio'=>encrypt($tasa->id)])}}">Editar</a> | 

	<a class="text-danger" href="{{ route('tasadecambio.destroy',['tasadecambio'=>encrypt($tasa->id)]) }}"
 	onclick="event.preventDefault();
 		document.getElementById('delete{{$tasa->id}}').submit();">
		Eliminar
	</a>
	<form id="delete{{$tasa->id}}" action="{{ route('tasadecambio.destroy',['tasadecambio'=>encrypt($tasa->id)]) }}" method="POST" enctype="multipart/form-data" style="display: none;">
		{{ method_field('DELETE')}}
  		{{ csrf_field()}}
	</form>

	</td>
</tr>
@endforeach
</tbody>
<tfoot>
<tr>
<th>#</th>
<th>Pais</th>
<th>TAX</th>
<th>Tasa de Cambio según Pais</th>
<th>Modena Local</th>
<th>Acciones</th>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
@stop