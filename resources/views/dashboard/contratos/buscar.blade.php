@extends('Templates.Dashboard.index')
@section('body')
<!-- Información -->
<?php 
$inicio = new dateTime($fechaInicio);
$fin = new dateTime($fechaFin);
$fechas = $fin->diff($inicio);
$cuidador = $fechas->d/6;
$cuidador = intval($cuidador);
$fechas->d = $fechas->d+1;
$resto = $fechas->d%6;
if($resto != 0){
  		$cuidador++;
  		
}



if($fechas->d > 6 ){

 ?>
<div class="alert alert-danger" role="alert">
  Usted a seleccionado un servicio por un aproximado de <?php echo $fechas->d ?> días, <strong>Recuerde: </strong> ustéd solo debe contratar un cuidador por un máximo de 6días continuos. <br>
  Para continuar debe seleccionar <strong><?php echo $cuidador; ?></strong> cuidadores.
</div>
<?php } ?>
 <!-- Left column start -->
<div class="alert alert-success" role="alert">
	Usted está contratando un servicio por una cantidad de <?php if($fechas->d == 0){ echo 1;}else{echo $fechas->d;} ?> días, para el cual usted deberá seleccionar <?php if($fechas->d == 0){ echo 1;}else{  echo $cuidador;} ?> cuidador(es), correspondiente a los días desde <strong><?php echo date('d-M',strtotime($fechaInicio)) ?></strong> hasta <strong><?php echo date('d-M',strtotime($fechaFin)) ?></strong>
	<br>
<div class="clear-fix"></div>
<div class="container row">
	<div class="col-lg-6">
		<div class="text-left">
			<a href="{{ url()->previous() }}"  style="color:white" class="btn btn-danger waves-effect waves-light btn-sm"> Cancelar
			</a>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="text-right">
			<button type="submit" class="btn btn-success waves-effect waves-light btn-sm" onclick="event.preventDefault();
 		document.getElementById('procederContrato').submit();"> Continuar
			</button>
		</div>
	</div>
</div>


</div>
<div class="row">

<div class="col-lg-9">
<form method="POST" id="procederContrato" action="{{route('contrato.update',['contrato' => encrypt(Auth::user()->id)])}}">
{{ method_field('PUT')}}
{{ csrf_field() }}
<!-- entradas heredadas -->
<input type="hidden" name="fechaInicio" value="{{ date('Y-m-d',strtotime($fechaInicio))}}">
<input type="hidden" name="fechaFin" value="{{ date('Y-m-d',strtotime($fechaFin))}}">
<input type="hidden" name="cuidador" value="{{ $cuidador }}">
<input type="hidden" name="estado" value="{{ $ciudad }}">
<input type="hidden" name="servicio" value="{{ $servicio }}">
<input type="hidden" name="costoDolar" value="{{ $costo[0]->costoDolar }}">
<input type="hidden" name="costoMonedaLocal" value="{{ $costo[0]->costoDolar * $costo[0]->tasaCambio}}">
<input type="hidden" name="MonedaLocal" value="{{ $costo[0]->monedaLocal}}">
<input type="hidden" name="dias" value="{{ $fechas->d }}">


<!-- fin de entradas heredadas -->
<div class="job-card card-columns">
<!-- Job card start -->
@foreach($user as $usuario)
<?php if($usuario->status == 3){ ?>
<?php if(!empty($servicio)){ ?>
<div class="card">
<div class="card-header">
<div class="media">
<a class="media-left media-middle" href="#">
<img class="media-object img-60" src="https://www.acompaño.com/users_images/{{$usuario->avatar}}" alt="Generic placeholder image">
</a>
<div class="media-body media-middle">
<div class="company-name">
<p>{{$usuario->name}} {{$usuario->lastName}}</p>
<span class="text-muted f-14">{{ date('M d, Y',strtotime($usuario->created_at))}}</span>
</div>
<?php 
$registro = new DateTime($usuario->created_at);
$hoy = new DateTime();
$tiempo = $hoy->diff($registro);
if($tiempo->d < 15){
?>
<div class="job-badge">
<label class="label bg-primary">Nuevo</label>
</div>
<?php } ?>
</div>
</div>
</div>
<div class="card-block">
<p class="text-muted">
<!-- aqui se introduce la descripción -->
</p>
<div class="job-lable">
<?php if(!empty($usuario->servicio->hospitales)){ ?><label class="label badge-success"> <?php echo 'Hospitales';?></label><?php } ?>
<?php if(!empty($usuario->servicio->domicilios)){ ?><label class="label badge-warning"> <?php echo 'Domicilios';?></label><?php } ?>
<?php if(!empty($usuario->servicio->sanatorios)){ ?><label class="label badge-danger"> <?php echo 'Sanatorio';?></label><?php } ?>
</div>
<div class="row">
<div class="col-lg-6">
<div class="job-meta-data"><i class="icofont icofont-safety"></i>{{ $usuario->estados->CiudadNombre}}</div>
<div class="job-meta-data"><i class="icofont icofont-university"></i><?php 
	$cumpleanos = new DateTime($usuario->BirthDay);
    $edad = $hoy->diff($cumpleanos);
    echo $edad->y;

 ?> años</div>
</div>
<div class="col-lg-6 text-right">
	<h4 class="precio" style="color: black"><?php echo $costo[0]->costoDolar * $costo[0]->tasaCambio . $costo[0]->monedaLocal?><small>/h</small></h4>
</div>
</div>
<div class="row">
	<div class="col-xs-7 row">
		<div class="col-lg-6"><label for="chack">Seleccionar </label></div>
		<div class="col-lg-5"><input id="check" type="checkbox" class="js-single" name="cuidadores[]" value="{{$usuario->id}}"/></div>
	</div>
	<div class="col-xs-4 text-right">
			<button type="button" class="btn btn-primary waves-effect waves-light btn-sm" data-toggle="modal" data-target="#usuarioModal{{$usuario->id}}"> Ver Perfil
		</button>
	</div>
</div>

</div>
</div>
<!-- Modal -->
<div class="modal fade" id="usuarioModal{{$usuario->id}}" tabindex="-1" role="dialog" aria-labelledby="usuarioModal{{ $usuario->id}}Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div style="height: 60rem"class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$usuario->name}} {{$usuario->lastName}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div style="height: 58rem" class="modal-body">
        <iframe style="width: 100%;height: 100%" src="{{route('users.show',['user'=>encrypt($usuario->id)])}}" frameborder="0"></iframe>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL -->
<?php } ?>
<?php } ?>
@endforeach
</div>
</form>
<?php 
$currentPage = $user->currentPage();
$maxPage = $currentPage + 5;
$firstPage = 1;
$lastPage = $user->lastPage();
$nextPage = $currentPage + 1 ;
$forwardPage = $currentPage - 1;
$user->setPath('');
?>
<!-- Pagination start -->
<nav aria-label="...">
<ul class="pagination justify-content-center m-t-20 m-b-20">
<li class="page-item <?php if($currentPage == $firstPage) : echo 'disabled'; endif ?> ">
<a class="page-link" href="
<?php  if($currentPage>1) : echo $user->url($forwardPage); else :   echo "#";  endif ?>" 
tabindex="-1">Anterior</a>
</li>
<?php for($x=$currentPage;$x<$maxPage;$x++){ ?>
	<?php if($x <= $lastPage){ ?>
	<li class="page-item <?php if($x ==$currentPage) : ?> active <?php endif ?>">
		<a class="page-link" href="<?php echo $user->url($x) ?>"> <?php echo $x; ?></a>
	</li>
	<?php } ?>
<?php } ?>
<li class="page-item <?php if($currentPage == $firstPage) : echo 'disabled'; endif ?>">
<a class="page-link" href="<?php if($currentPage < $lastPage) : echo $user->url($x); else : echo "#"; endif ?>">Siguiente</a>
</li>
</ul>
</nav>
<!-- Pagination end -->
</div>

<!-- Left column end -->
<!-- Right column start -->
<div class="col-lg-3 col-md-12">
<!-- Filter card start -->
<div class="card">
<div class="card-header">
<h5><i class="icofont icofont-filter m-r-5"></i>Filtro</h5>
</div>
<div class="card-block">
<form action="{{route('contrato.show',['contrato'=>encrypt($id)])}}" role='search' method="GET">
<div class="form-group row">
<div class="col-sm-12">
<select class="form-control" name="ciudad">
<option>Seleccione la Ciudad</option>
<?php foreach ($ciudades as $ciudad) { ?>
	<option value="{{$ciudad->CiudadID}}"> {{$ciudad->CiudadNombre}}</option>
<?php } ?>
</select>
</div>
</div>
<div class="form-group row">
<div class="col-sm-12">
<select class="form-control" name="servicio">
<option>Seleccione el tipo de servicio</option>
<option value="1">Servicio en Domicilio</option>
<option value="2">Servicio en Hospitales</option>
<option value="3">Servicio en Sanatorio</option>
</select>
</div>
</div>
<div class="text-right">
<button type="submit" class="btn btn-primary">
<i class="icofont icofont-job-search m-r-5"></i> Cuidador
</button>
</div>
</form>
</div>
</div>
<!-- Filter card end -->
<!-- Location card start -->

<!-- Location card end -->
<!-- Job Title card start -->

<!-- Job Title card end -->
<!-- Recent Searches card end -->
</div>
</div>
</div>


 @stop