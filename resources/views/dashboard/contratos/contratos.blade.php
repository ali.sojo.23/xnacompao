@extends('Templates.Dashboard.index')
@section('body')
<!-- Información -->
<div class="row">

<div class="col-lg-12">

<div class="job-card card-columns">
@foreach($contratos as $contrato)
<?php if($contrato->user_id == Auth::user()->id){ ?>
<div class="col-sm-12">
<div class="card card-border-primary">
<div class="card-header">
<h5><?php echo Auth::user()->name ?> <?php echo Auth::user()->lastName ?></h5>
<!-- <span class="label label-default f-right"> 28 January, 2015 </span> -->
</div>
<div class="card-block">
<div class="row">
<div class="col-sm-6">
<ul class="list list-unstyled">
<li>Factura #: ACOMPA-<?php echo $contrato->factura->id ?></li>
<li>Facturado: <span class="text-semibold"><?php echo date('Y/m/d',strtotime($contrato->factura->created_at)) ?></span></li>
</ul>
</div>
<div class="col-sm-6">
<ul class="list list-unstyled text-right">
<li><?php echo  $contrato->factura->total . " " . $contrato->factura->monedaLocal  ?></li>
<li>Servicio: <span class="text-semibold">
<?php if($contrato->servicio == 1){ echo "Domicilio";} ?>
<?php if($contrato->servicio == 2){ echo "Hospitales";} ?>
<?php if($contrato->servicio == 3){ echo "Sanatorios";} ?>
</span></li>
</ul>
</div>
</div>
</div>
<div class="card-footer">
<div class="task-board m-0">
<a href="{{ route('invoice.show',['invoice'=>encrypt($contrato->id)])}}" class="btn btn-info btn-mini b-none"><i class="icofont icofont-eye-alt m-0"></i></a>
<!-- end of dropdown-secondary -->
</div>
</div>
</div>
</div>
<?php } ?>
@endforeach

</div>
</div>
</div>


 @stop