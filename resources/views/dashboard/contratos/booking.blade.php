@extends('Templates.Dashboard.users')
@section('body')
<div class="card">
<div class="card-header">
<h5>¡Contrata un Cuidador ahora!</h5>
<span>Seleccione los campos necesarios para proceder con la contratación. <br>
recuerde que si usted requiere de mas de 6 días continuos debe contratatar a 2 personas distintas</span>
</div>
<div class="card-block">
<div class="row">
<div class="col-md-12">
<div id="wizard">
<section>
<form class="wizard-form" id="example-advanced-form" method="GET" action="{{route('contrato.show',['contrato'=>encrypt(Auth::user()->country)])}}" enctype="multipart/form-data">
{{ csrf_field() }}
<h3> Contratación </h3>
<fieldset>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="email-2" class="block">Tipo de servicio a contratar</label>
</div>
<div class="col-md-8 col-lg-10">
	<select class="form-control" name="servicio">
			<option>Seleccione el tipo de servicio</option>
			<option value="1">Servicio en Domicilio</option>
			<option value="2">Servicio en Hospitales</option>
			<option value="3">Servicio en Sanatorio</option>
	</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="email-2" class="block">Seleccione la ciudad a contratar</label>
</div>
<div class="col-md-8 col-lg-10">
<select class="form-control" name="ciudad">
<option>Seleccione la Ciudad</option>
<?php foreach ($ciudades as $ciudad) { ?>
	<?php if($ciudad->PaisCodigo == Auth::user()->country){ ?>
	<option value="{{$ciudad->CiudadID}}"> {{$ciudad->CiudadNombre}}</option>
<?php }} ?>
</select>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="password-2" class="block">Fecha de inicial</label>
</div>
<div class="col-md-8 col-lg-10">
<input type="date" class="form-control date-control" name="fechaInicioContrato" value="<?php echo date("Y-m-d");?>" min=<?php echo date("Y-m-d");?>>
</div>
</div>
<div class="form-group row">
<div class="col-md-4 col-lg-2">
<label for="confirm-2" class="block">Fecha de final</label>
</div>
<div class="col-md-8 col-lg-10">
<input type="date" class="form-control date-control" name="fechaFinContrato" value="<?php echo date("Y-m-d");?>" min=<?php echo date("Y-m-d");?>>
</div>
</div>
</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>


@stop