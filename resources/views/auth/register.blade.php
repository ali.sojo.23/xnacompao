@extends('Templates.auth.loginRegister')

@section('content')

<section class="login p-fixed d-flex text-center bg-primary common-img-bg">

<div class="container-fluid">
<div class="row">
<div class="col-sm-12">

<div class="signup-card card-block auth-body mr-auto ml-auto">
	
<form class="md-float-material" method="POST" action="{{ route('register') }}">
{{ csrf_field() }}
<div class="text-center">
<img src="../files/assets/images/logo.png" alt="logo.png">
</div>
<div class="auth-box">
<div class="row m-b-20">
<div class="col-md-12">

<h3 class="text-center txt-primary">Registrarse en nuestro sitio es muy fácil</h3>
</div>
</div>
<div class="input-group">
<select class="form-control" id="FormControlSelect1" name="role">
    <option disabled="" selected="selected">-- Seleccione tipo de usuario --</option>}
    option
    <option value="1">Cuidador</option>
    <option value="2">Cliente</option>
</select>
<span class="md-line"></span>
</div>
<div class="input-group">
<select class="form-control" id="FormControlSelect2" name="country">
    <option disabled="" selected="selected">-- Seleccione su País --</option>}
    option
    @foreach($pais as $pais)
        @if($pais->PaisContinente =='North America'|| $pais->PaisContinente == 'South America')
    <option value="{{ $pais->PaisCodigo}}">{{$pais->PaisNombre}}</option>
        @endif
    @endforeach
</select>
<span class="md-line"></span>
</div>
<div class="input-group">
<input type="text" class="form-control" placeholder="Nombre" name="name" value="{{ old('name') }}" required>
@if ($errors->has('name'))
    <span class="help-block text-warning">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
@endif
<span class="md-line"></span>
</div>
<div class="input-group">
<input type="text" class="form-control" placeholder="Apellidos" name="lastName" value="{{ old('lastName') }}" required>
@if ($errors->has('lastName'))
    <span class="help-block text-warning">
        <strong>{{ $errors->first('lastName') }}</strong>
    </span>
@endif
<span class="md-line"></span>
</div>
<div class="input-group">
<input type="text" class="form-control" placeholder="correo electrónico" name="email" value="{{ old('email') }}" required>
@if ($errors->has('email'))
    <span class="help-block text-warning">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
@endif
<span class="md-line"></span>
</div>
<div class="input-group">
<input type="password" class="form-control" placeholder="Contraseña" name="password" required>
@if ($errors->has('password'))
    <span class="help-block text-warning">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
@endif
<span class="md-line"></span>
</div>
<div class="input-group">
<input type="password" class="form-control" placeholder="Confirmar Contraseña"  name="password_confirmation" required>
<span class="md-line"></span>
</div>
<div class="row m-t-25 text-left">
<div class="col-md-12">
<div class="checkbox-fade fade-in-primary">
<label>
<input type="checkbox"name="terms" unchecked>
<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
<span class="text-inverse">He leido y acepto todos los  <a href="#">Terminos &amp; Condiciones.</a></span>
</label>
</div>
</div>
</div>
<div class="row m-t-30">
<div class="col-md-12">
<button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Registrar Ahora</button>
</div>

</div>
<div class="forgot-phone text-right f-right">
<a href="{{ route('login') }}" class="text-right f-w-600 text-inverse"> Ya tengo una cuenta en acompaño.com</a>
<br>
</div>
</div>
</form>

</div>

</div>

</div>

</div>

</section>
@stop