<form class="md-float-material" method="POST" action="{{ route('password.email') }}">
{{ csrf_field() }}
<div class="text-center d-none d-lg-block  d-xl-block">
<img src="../files/assets/images/logo.png" alt="logo.png">
</div>
<div class="auth-box">
<div class="row m-b-20">
<div class="col-md-12">

<h3 class="text-center txt-primary">Recuperar Contraseña</h3>
</div>

</div>
 @if (session('status'))
	<div class="alert alert-success">
        {{ session('status') }}
    </div> 
@endif
<div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
<input type="email" class="form-control" placeholder="Introduzca su correo electrónico" name="email" value="{{ old('email') }}">
 @if ($errors->has('email'))
 <br>
     <span class="help-block text-warning">
         <strong>{{ $errors->first('email') }}</strong>
     </span>
 @endif
<span class="md-line"></span>
</div>
<div class="row m-t-30">
<div class="col-md-12">
<button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Send Password Reset Link</button>
</div>
</div>
</div>
</form>