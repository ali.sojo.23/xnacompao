<form class="md-float-material" method="POST" action="{{ route('login') }}">
{{ csrf_field() }}
<div class="text-center d-none d-lg-block  d-xl-block">
<img src="../files/assets/images/logo.png" alt="logo.png">
</div>
<div class="auth-box">
<div class="row m-b-20">
<div class="col-md-3">

<h3 class="text-center txt-primary">Iniciar Sesión</h3>
</div>
<div class="col-md-9">
<p class="text-inverse m-t-25 text-left">¿No tienes una cuenta en <a href="/">acompaño.com<a>? <a href="/register"> ¡Registrate </a> aquí es gratis!</p>
</div>
</div>
@if(session('status'))
<p class="text-inverse b-b-default text-left p-b-5">
	
		<strong>{{session('status')}}</strong>

</p>
@endif
<div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
<input type="email" class="form-control" placeholder="Introduzca su correo electrónico" name="email" value="{{ old('email') }}">
 @if ($errors->has('email'))
 <br>
     <span class="help-block text-warning">
         <strong>{{ $errors->first('email') }}</strong>
     </span>
 @endif
<span class="md-line"></span>
</div>
<div class="input-group{{ $errors->has('password') ? ' has-error' : '' }}">
<input type="password" class="form-control" placeholder="Contraseña" name="password">
@if ($errors->has('password'))
<br>
    <span class="help-block text-warning">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
@endif
<span class="md-line"></span>
</div>
<div class="row m-t-25 text-left">
<div class="col-12">
<div class="checkbox-fade fade-in-primary">
<label>
<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
<span class="text-inverse">Recuerdame</span>
</label>
</div>
<div class="forgot-phone text-right f-right">
<a href="{{ route('password.request') }}" class="text-right f-w-600 text-inverse"> ¿Olvidó su contraseña?</a>
</div>
</div>
</div>
<div class="row m-t-30">
<div class="col-md-12">
<button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Iniciar Sesión</button>
</div>
</div>
</div>
</form>