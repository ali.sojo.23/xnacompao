@extends('Templates.auth.loginRegister')

@section('content')

    
<section class="login header p-fixed d-flex text-center bg-primary common-img-bg">

<div class="container-fluid">
<div class="row">
<div class="col-sm-12">

<div class="signin-card card-block auth-body mr-auto ml-auto">
@include('auth.assets.login')

</div>

</div>

</div>

</div>




@stop