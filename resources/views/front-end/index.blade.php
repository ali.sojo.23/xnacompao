<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en-us" class="no-js">
<head>
<meta charset="utf-8">
<title>acompaño.com | El servicio de Cuidadores más grande de América </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Go In Inc.">
<meta name="description" content="acompaño.com es la primera empresa en proveer servicios de Cuidador en Domicilio, Sanatorio, Hospital Público y Privado; Ubicados en latinoamerica y en perido de expansión. con facilidades de contratación a travéz de su dispositivo movil con solo dar un click en nuestra aplicación.">
<meta name="keyword" content="servicio de acompañantes, medicina a domicilio, medicina en casa, cuidador medico, enfermero en casa, enfermeria, cuidados para la salud">
<link rel="shortcut icon" href="files/assets/images/favicon.png">
<link rel="stylesheet" href="css/style-v2.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script src="js/modernizr.custom.js"></script>
</head>
<body>

<div id="loading" class="dark-back">
<div class="loading-bar"></div>
<span class="loading-text opacity-0"><img style="max-height: 100px;" src="files/assets/images/logo.gif" alt=""></span>
</div>

<div class="info-bar bar-intro opacity-0">
<div class="row">
<div class="col-xs-12 col-sm-6 col-lg-6 info-bar-left">
<!-- <p>Lanzamiento de la App en solo: <span id="countdown"></span></p> -->
</div>
<div class="col-xs-12 col-sm-6 col-lg-6 info-bar-right">
<a data-dialog="somedialog" class="action-btn trigger">Iniciar Sesión</a>
<a href="{{route('register')}}" class="action-btn trigger">Registrate</a>
</div>
</div>
</div>


<div id="slider" class="sl-slider-wrapper">
<div class="sl-slider">

<div class="sl-slide bg-1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
<div class="sl-slide-inner">
<div class="content-slide">
<div class="container" id="contenedor">
<h1 class="text-intro opacity-0"><img style="margin: 0 auto;" class="img-responsive" src="files/assets/images/logoGrande.png" alt=""></h1>
<p class="text-intro opacity-0">Estamos contigo!! <strong><a style="color:blue" href="https://acompaño.com">Te Acompañamos</a></strong> donde quiera que estes.
<br> Somos tu familia - Con cuidadores calificados.
</p>



</div>
</div>
</div>
</div>


<div class="sl-slide bg-2" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
<div class="sl-slide-inner">
<div class="content-slide">
<div class="container">
<h2>¿Quienes somos?</h2>
<br><br>
<div class="row about-part">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<p class="text-justify">
Acompaño es una empresa que se mezcla con la sociedad para garantizarles el tener compañía en sus momentos de convalecencia, brindado un servicio de calidad y calidez humana para aquellas familias que no pueden dejar a uno de sus queridos miembros desatendido.
</p>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<p class="text-justify">
Con una evaluación minuciosa a cada uno de sus afiliados, Acompaño se enfoca en disminuir su preocupación por conseguir un acompañante de confianza, adecuado para su ser querido, con vocación de servicio, aprecio por la vida y gran amabilidad. 	
</p>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<p class="text-justify">
Se especializa en el servicio de cuidadores en pro de ayudar a luchar contra los problemas de salud y de cuidados especiales. Además de un compromiso es amor al cuidado de personas que lo necesitan y dedicación al campo de la salud
</p>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<p class="text-justify">
Acompaño lleva a otro nivel el servicio de cuidador. Se convierte en su segunda familia, su familia sustituta y está con usted para formar el pilar de apoyo que usted necesita. 
</p>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="sl-slide bg-3" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
<div class="sl-slide-inner">
<div class="content-slide">
<div class="container">
<h2>¿Por qué descargar la aplicación?</h2>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<p class="text-justify">
	Además de una empresa dedicada al cuidado de personas, Acompaño es una aplicación disponible para ti en el momento necesario, aquí y ahora. Presentamos una forma fácil de encontrarnos y de solicitar un acompañante calificado que ha sido previamente evaluado y es monitoreado por nosotros para brindar un servicio dedicado a familias con miembros enfermos o propensos a enfermarse.
</p>
<br>
<h3>Beneficios de la App de acompaño:</h3>
<br>
<p class="text-left">
• Ofrecemos atención inmediata <br>
• Vamos contigo a donde vayas <br>
• Presentamos diversas propuestas de cuidadores especializados <br>
• Aseguramos un buen servicio <br>
• Es de fácil acceso <br>
</p>	
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

</div>
</div>
</div>
</div>
</div>


<div class="sl-slide bg-4" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25" data-slice1-scale="2" data-slice2-scale="1">
<div class="sl-slide-inner">
<div class="content-slide">
<div class="container">
<h2>Nuestros Servicios</h2>
<p class="resume"><a style="color:blue;" href="https://acompaño.com">acompaño.com</a> es la primera empresa en proveer servicios de Cuidador en Domicilio, Sanatorio, Hospital Público y Privado; Ubicados en latinoamerica y en perido de expansión. con facilidades de contratación a travéz de su dispositivo movil con solo dar un click en nuestra aplicación.</p>
<div class="row services">

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 box-info">
<div class="box-info-light">
<span class="icon"><i class="fas fa-medkit"></i></span>
<h3>Cuidador en Sanatorio</h3>
<p>
<ul>
    <li style="color:black ">• Contigo en sanatorios, hospitales públicos y privados.</li>
    <li style="color:black ">• En cada momento necesario prestamos asistencia y cuidados.</li>
    <li style="color:black ">• Formación de nexo entre los familiares, allegados y personal del recinto médico.</li>
</ul>      
</p>
</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 box-info">
<div class="box-info-light">
<span class="icon icon-clock"><i class="fas fa-hand-holding-heart"></i></span>
<h3>Cuidador en Domicilio</h3>
<p>
<ul>
    <li style="color:black ">• Asistencia y apoyo durante su estado de convalecencia.</li>
    <li style="color:black ">• Continuidad de la labor y cuidados posteriores a la internación.</li>
    <li style="color:black ">• Visita médica de apoyo</li>
</ul>
</p>
</div>
</div>
</div>
<div class="row services">

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 box-info">
<div class="box-info-light">
<span class="icon"><i class="fas fa-user-md"></i></span>
<h3>Domicilio Especial</h3>
<p>
<ul>
    <li style="color:black ">• Compañía en tiempos de recuperación de una enfermedad aguda o accidente.</li>
    <li style="color:black ">• Apoyo para la realización de terapias recomendadas por el médico (caminatas, ejercicios, consultas médicas), a excepción de apoyo en procedimientos de enfermería.</li>
    <li style="color:black ">• No es necesario haber pasado por un procedimiento médico, internación o enfermedad.</li>
    <li style="color:black ">• Visita médica de apoyo</li>
</ul>
</p>
</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 box-info">
<div class="box-info-light">
<span class="icon"><i class="fas fa-heartbeat"></i></span>
<h3>Sanatorio Plus</h3>
<p>
<ul>
    <li style="color:black ">• Dedicado a los socios de Sanatorio Calificado que lo contraten.</li>
    <li style="color:black ">• Cobertura de servicios en internación domiciliaria.</li>
    <li style="color:black ">• Cobertura horaria: 8, 12, 16 o 24 horas.</li>
    <li style="color:black ">• Visita médica de apoyo</li>
</ul>
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="sl-slide bg-5" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1">
<div class="sl-slide-inner">
<div class="content-slide">
<div class="container">
<h2>¿Requieres asistencia? Comunícate con nosotros</h2>
<p class="resume">Si quieres formar parte de Acompaño,<strong> simplemente registrate</strong> e inicia sesión o descarga nuestra aplicación.
<br> Si tienes dudas, realiza tu consulta y el equipo de Acompaño te atenderá.</p>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 info-contact">
<h3>Departamento de Servicios</h3>
<p>Si desea hacer alguna sugerencia, preguna o comentario, esta es la mejor forma de encontrarnos.</p>
<br>
<!-- <p class="list-info">
<i class="icon ion-ios-telephone"></i> Teléfono : (+33) 66-1254-611
<br>
<i class="icon ion-ios-email"></i> Correo electrónico : <a href="/cdn-cgi/l/email-protection#98e1f7edeab5fdf5f9f1f4d8fde0f9f5e8f4fdb6fbf7f5" class="phone-mail-link"><span class="__cf_email__" data-cfemail="b5d1c0d8ccf5d0cdd4d8c5d9d09bd6dad8">[email&#160;protected]</span></a>
<br>
<i class="icon ion-ios-location"></i> Dirección : 66 Grand Central, NY 66564, USA
</p> -->
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-offset-1">

<form id="contact-form" name="contact-form" method="POST" data-name="Contact Form">
<div class="row">

<div class="col-xs-12 col-sm-12 col-lg-6">
<div class="form-group">
<input type="text" id="name" class="form form-control" placeholder="Tu nombre*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Name*'" name="name" data-name="Name" required>
</div>
</div>

<div class="col-xs-12 col-sm-12 col-lg-6">
<div class="form-group">
<input type="email" id="email" class="form form-control" placeholder="Tu correo electrónico*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email*'" name="email-address" data-name="Email Address" required>
</div>
</div>

<div class="col-xs-12 col-sm-12 col-lg-12">
<div class="form-group">
<input type="text" id="subject" class="form form-control" placeholder="Saludo..." onfocus="this.placeholder = ''" onblur="this.placeholder = 'Regarding...'" name="subject" data-name="Subject">
</div>
</div>

<div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
<div class="form-group">
<textarea id="text-area" class="form textarea form-control" placeholder="Escriba su mensaje aquí... 20 caracteres como mínimo.*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your message here... 20 characters Min.*'" name="message" data-name="Text Area" required></textarea>
</div>
<span class="sub-text">* Campos requeridos</span>
</div>
</div>

<button type="submit" id="valid-form" class="btn btn-color">Enviar mi mensaje</button>
</form>


<div id="block-answer">
<div id="answer"></div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>

</div>


<nav id="nav-arrows" class="nav-arrows">
<span class="nav-arrow-prev"></span>
<span class="nav-arrow-next"></span>
</nav>


<nav id="nav-multi-square" class="nav-multi-square nav-intro opacity-0">
<span class="nav-square-current nav-square">
<span><i class="fa fa-home"></i>Inicio</span>
</span>
<span class="nav-square">
<span><i class="far fa-handshake"></i>Nosotros</span>
</span>
<span class="nav-square">
<span><i class="fas fa-mobile-alt"></i>APP Mobile</span>
</span>
<span class="nav-square">
<span><i class="fa fa-list-ol"></i>Servicios</span>
</span>
<span class="nav-square">
<span><i class="fas fa-mail-bulk"></i>Contactanos</span>
</span>
</nav>

</div>


<div id="somedialog" class="dialog">
<div class="dialog__overlay"></div>

<div class="dialog__content">
<div class="header-picture"></div>


<div class="dialog-inner">
<h4>Inicia Sesion</h4>


<div id="subscribe">
<form action="{{ route('login') }}" class="notifyMe" method="POST">
	{{ csrf_field() }}
<div class="form-group">
<div class="controls">
 @if ($errors->has('email'))
 <br>
     <span class="help-block text-warning">
         <strong style="color: red;">{{ $errors->first('email') }}</strong>
     </span>
 @endif
<div class="{{ $errors->has('email') ? ' has-error' : '' }}">
<input type="email" class="form-control" placeholder="Introduzca su correo electrónico" name="email" value="{{ old('email') }}">

</div>
<div class="{{ $errors->has('password') ? ' has-error' : '' }}">
<input type="password" class="form-control" placeholder="Contraseña" name="password">
@if ($errors->has('password'))
<br>
    <span class="help-block text-warning">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
@endif

<i class="fa fa-spinner opacity-0"></i>

<button type="submit" class="btn btn-lg submit">iniciar sesión</button>
<div class="clear"></div>
</div>
</div>
</form>

<div class="block-message">
<div class="message">
<p class="notify-valid"></p>
</div>
</div>

</div>

</div>



<button class="close-newsletter" data-dialog-close><i class="icon ion-android-close"></i></button>
</div>

</div>


<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

<div class="pswp__bg"></div>

<div class="pswp__scroll-wrap">

<div class="pswp__container">
<div class="pswp__item"></div>
<div class="pswp__item"></div>
<div class="pswp__item"></div>
</div>

<div class="pswp__ui pswp__ui--hidden">
<div class="pswp__top-bar">

<div class="pswp__counter"></div>
<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
<button class="pswp__button pswp__button--share" title="Share"></button>
<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>


<div class="pswp__preloader">
<div class="pswp__preloader__icn">
<div class="pswp__preloader__cut">
<div class="pswp__preloader__donut"></div>
</div>
</div>
</div>
</div>
<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
<div class="pswp__share-tooltip"></div>
</div>
<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
</button>
<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
</button>
<div class="pswp__caption">
<div class="pswp__caption__center"></div>
</div>
</div>
</div>
</div>





<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/jquery.min.js"></script>
<script src="js/jquery.easings.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/jquery.ba-cond.min.js"></script>
<script src="js/jquery.slitslider.js"></script>

<script src="js/notifyMe.js"></script>

<script src="js/contact-me.js"></script>

<script src="js/classie.js"></script>
<script src="js/dialogFx.js"></script>

<script src="js/photoswipe.js"></script>

<script src="js/photoswipe-ui-default.js"></script>

<script src="js/jquery.mCustomScrollbar.js"></script>

<script src="js/jquery.countdown.js"></script>
<script>
    $("#countdown")
        // Year/Month/Day Hour:Minute:Second
        .countdown("2019/01/01 15:30:30", function(event) {
            $(this).html(
                event.strftime('%D Dias %Hh %Mm %Ss')
            );
        });
    </script>

<script src="js/main.js"></script>
</body>
</html>
