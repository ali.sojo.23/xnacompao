<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
            @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
                body[yahoo] .buttonwrapper { background-color: transparent !important; }
                body[yahoo] .button { padding: 0 !important; }
                body[yahoo] .button a { background-color: #9b59b6; padding: 15px 25px !important; }
            }

            @media only screen and (min-device-width: 601px) {
                .content { width: 600px !important; }
                .col387 { width: 387px !important; }
            }
        </style>
</head>
<body bgcolor="#34495E" style="margin: 0; padding: 0;" yahoo="fix">
<!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td>
        <![endif]-->
<table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; max-width: 600px;" class="content">
<tr>
<td style="padding: 15px 10px 15px 10px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="center" style="color: #fff; font-family: Arial, sans-serif; font-size: 12px;">

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" bgcolor="#0073AA " style="padding: 20px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 36px; font-weight: bold;">
<img src="https://www.acompaño.com/files/assets/images/logo.png" alt="Logo acompaño.com" width="152" style="display:block;" />
</td>
</tr>
<tr>
<td align="center" bgcolor="#ffffff" style="padding: 75px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
<h2>Confirma Tu Correo electronico</h2><br/>
<b>{{$user->email}}</b><br><br>
Muchas gracias {{$user->name}} {{$user->lastName}} por registrarte en nuestros servicios de acompañantes de <a href="https://acompaño.com" style="text-decoration: none;color:black;">acompaño.com</a>.
<br>
<br> 
Estas a solo un click para comenzar a trabajar con nosotros.
 
</td>
</tr>
<tr>
<td align="center" bgcolor="#f9f9f9" style="padding: 30px 20px 30px 20px; font-family: Arial, sans-serif;">
<table bgcolor="#0073AA" border="0" cellspacing="0" cellpadding="0" class="buttonwrapper">
<tr>
<td align="center" height="50" style=" padding: 0 25px 0 25px; font-family: Arial, sans-serif; font-size: 16px; font-weight: bold;" class="button">
<a href="{{route('sendEmailDone',['email' => $user->email, 'verifyToken'=>$user->verifyToken])}}" style="color: #ffffff; text-align: center; text-decoration: none;">Click aquí para verificar</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center" bgcolor="#dddddd" style="padding: 15px 10px 15px 10px; color: #555555; font-family: Arial, sans-serif; font-size: 12px; line-height: 18px;">
<b>&copy; <a href="https://acompaño.com" title="">Acompaño.com</a></b>
</td>
</tr>
<tr>
<td style="padding: 15px 10px 15px 10px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td align="center" width="100%" style="color: #fff; font-family: Arial, sans-serif; font-size: 12px;">
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--[if (gte mso 9)|(IE)]>
                </td>
            </tr>
        </table>
        <![endif]-->
</body>
</html>