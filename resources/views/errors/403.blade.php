@extends('Templates.errors.errors')
@section('content')
<section class="login offline-404 p-fixed d-flex text-center">

<div class="container-fluid">
<div class="row">
<div class="col-sm-12">

<div class="auth-body">
<form>
<h1>403</h1>
<h2>Oops! Acceso no autorizado</h2>
<div class="left-icon-control">
<input type="text" class="form-control form-control-lg" placeholder="Try a new Search">
<div class="form-icon">
<i class="icofont icofont-search"></i>
</div>
</div>
<a href="index.html" class="btn btn-primary btn-lg m-t-30">Back to Home</a>
</form>
</div>

</div>
</div>
</div>
</section>


@stop